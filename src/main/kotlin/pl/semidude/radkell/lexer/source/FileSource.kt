package pl.semidude.radkell.lexer.source

import java.io.File

/**
 * [Source] implementation, which loads the source code from file
 */
class FileSource(path: String) : StringSource(File(path).bufferedReader().readText())
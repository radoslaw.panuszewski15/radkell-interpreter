package pl.semidude.radkell.parser.evaluation

import pl.semidude.radkell.exception.RadkellRuntimeException
import pl.semidude.radkell.parser.RuntimeParamDef

/**
 * Basic interface for objects that can evaluate values from themselves
 */
interface Evaluable {
    fun evaluateInt(context: CallContext): Int = throw IllegalStateException("cannot evaluate Int from ${this.javaClass}")
    fun evaluateDouble(context: CallContext): Double = throw IllegalStateException("cannot evaluate Double from ${this.javaClass}")
    fun evaluateBool(context: CallContext): Boolean = throw IllegalStateException("cannot evaluate Boolean from  type${this.javaClass}")
    fun evaluateIntList(context: CallContext): List<Int> = throw IllegalStateException("cannot evaluate [Int] from  ${this.javaClass}")
    fun evaluateDoubleList(context: CallContext): List<Double> = throw IllegalStateException("cannot evaluate [Double] from ${this.javaClass}")
    fun evaluateBoolList(context: CallContext): List<Boolean> = throw IllegalStateException("cannot evaluate [Bool] from ${this.javaClass}")
}

/**
 * Evaluate value of given function parameter or input slot, by querying the [CallContext]
 */
class ContextParamReader(
    private val paramDef: RuntimeParamDef
) : Evaluable {

    override fun evaluateInt(context: CallContext): Int =
        context.getIntParam(paramDef)

    override fun evaluateDouble(context: CallContext): Double =
        context.getDoubleParam(paramDef)

    override fun evaluateBool(context: CallContext): Boolean =
        context.getBoolParam(paramDef)

    override fun evaluateIntList(context: CallContext): List<Int> =
        context.getIntListParam(paramDef)

    override fun evaluateDoubleList(context: CallContext): List<Double> =
        context.getDoubleListParam(paramDef)

    override fun evaluateBoolList(context: CallContext): List<Boolean> =
        context.getBoolListParam(paramDef)
}

/**
 * Simply evaluates the given [Evaluable]
 */
class SimpleEvaluator(private val evaluable: Evaluable)
    : LazyEvaluator(
    evaluableProvider = { evaluable }
)

/**
 * Evaluates given evaluable in a lazy way. The laziness means that the evaluable can change in time, thus it's
 * obtained via evaluableProvider
 */
open class LazyEvaluator(
    /**
     * Function used to obtain instance of [Evaluable] to be evaluated
     */
    private val evaluableProvider: (CallContext) -> Evaluable,
    /**
     * Function used to do some optional preprocessing of [CallContext] before the evaluation is performed
     */
    private val contextPreprocessor: (CallContext) -> CallContext = { it }
) : Evaluable {

    override fun evaluateInt(context: CallContext): Int =
        evaluableProvider(context).evaluateInt(contextPreprocessor.invoke(context))

    override fun evaluateDouble(context: CallContext): Double =
        evaluableProvider(context).evaluateDouble(contextPreprocessor.invoke(context))

    override fun evaluateBool(context: CallContext): Boolean =
        evaluableProvider(context).evaluateBool(contextPreprocessor.invoke(context))

    override fun evaluateIntList(context: CallContext): List<Int> =
        evaluableProvider(context).evaluateIntList(contextPreprocessor.invoke(context))

    override fun evaluateDoubleList(context: CallContext): List<Double> =
        evaluableProvider(context).evaluateDoubleList(contextPreprocessor.invoke(context))

    override fun evaluateBoolList(context: CallContext): List<Boolean> =
        evaluableProvider(context).evaluateBoolList(contextPreprocessor.invoke(context))
}

/**
 * Utility evaluator used to quickly evaluate some value from list, by applying the given reducer function.
 */
class ListReducerEvaluator(
    private val evaluable: Evaluable,
    private val reducer: (List<Any>) -> Any
) : Evaluable {

    override fun evaluateInt(context: CallContext): Int =
        processList(evaluable.evaluateIntList(context))

    override fun evaluateDouble(context: CallContext): Double =
        processList (evaluable.evaluateDoubleList(context))

    override fun evaluateBool(context: CallContext): Boolean =
        processList (evaluable.evaluateBoolList(context))

    override fun evaluateIntList(context: CallContext): List<Int> =
        processList(evaluable.evaluateIntList(context))

    override fun evaluateDoubleList(context: CallContext): List<Double> =
        processList(evaluable.evaluateDoubleList(context))

    override fun evaluateBoolList(context: CallContext): List<Boolean> =
        processList(evaluable.evaluateBoolList(context))

    private inline fun <reified T> processList(list: List<Any>): T =
        if (list.isNotEmpty())
            reducer(list) as T
        else
            throw RadkellRuntimeException(0, "colon operator used on empty list") //TODO line number
}
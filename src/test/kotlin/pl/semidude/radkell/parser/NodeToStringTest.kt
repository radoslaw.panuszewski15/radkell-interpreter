package pl.semidude.radkell.parser

import org.junit.Test
import pl.semidude.radkell.lexer.TokenDescriptor
import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.nodes.*
import kotlin.test.assertEquals

class NodeToStringTest {

    //LiteralNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert IntLiteralNode to string`() {
        assertEquals(
            "IntLiteralNode[ 8 ]",
            IntLiteralNode(8).toString()
        )
    }

    @Test
    fun `test convert DoubleLiteralNode to string`() {
        assertEquals(
            "DoubleLiteralNode[ 8.0 ]",
            DoubleLiteralNode(8.0).toString()
        )
    }

    @Test
    fun `test convert BoolLiteralNode to string`() {
        assertEquals(
            "BoolLiteralNode[ true ]",
            BoolLiteralNode(true).toString()
        )
    }

    @Test
    fun `test convert IntListLiteralNode to string`() {
        assertEquals(
            "IntListLiteralNode[ [1, 2, 3] ]",
            IntListLiteralNode(listOf(1, 2, 3)).toString()
        )
    }

    @Test
    fun `test convert DoubleListLiteralNode to string`() {
        assertEquals(
            "DoubleListLiteralNode[ [1.0, 2.0, 3.0] ]",
            DoubleListLiteralNode(listOf(1.0, 2.0, 3.0)).toString()
        )
    }

    @Test
    fun `test convert BoolListLiteralNode to string`() {
        assertEquals(
            "BoolListLiteralNode[ [true, false, true] ]",
            BoolListLiteralNode(listOf(true, false, true)).toString()
        )
    }

    //ArithmeticNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert AddNode to string`() {
        assertEquals(
            "AddNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ]",
            AddNode(IntLiteralNode(1), IntLiteralNode(2), 0).toString()
        )
    }

    @Test
    fun `test convert SubtractNode to string`() {
        assertEquals(
            "SubtractNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ]",
            SubtractNode(IntLiteralNode(1), IntLiteralNode(2)).toString()
        )
    }

    @Test
    fun `test convert MultiplyNode to string`() {
        assertEquals(
            "MultiplyNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ]",
            MultiplyNode(IntLiteralNode(1), IntLiteralNode(2)).toString()
        )
    }

    @Test
    fun `test convert DivideNode to string`() {
        assertEquals(
            "DivideNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ]",
            DivideNode(IntLiteralNode(1), IntLiteralNode(2)).toString()
        )
    }

    @Test
    fun `test convert UnaryMinusNode to string`() {
        assertEquals(
            "UnaryMinusNode[ IntLiteralNode[ 8 ] ]",
            UnaryMinusNode(IntLiteralNode(8)).toString()
        )
    }

    //LogicalNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert OrNode to string`() {
        assertEquals(
            "OrNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ] ] ]",
            OrNode(listOf(BoolLiteralNode(true), BoolLiteralNode(false))).toString()
        )
    }

    @Test
    fun `test convert AndNode to string`() {
        assertEquals(
            "AndNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ] ] ]",
            AndNode(listOf(BoolLiteralNode(true), BoolLiteralNode(false))).toString()
        )
    }

    @Test
    fun `test convert equals CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] == IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.EQUALS_OP).toString()
        )
    }

    @Test
    fun `test convert not-equals CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] != IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.NOT_EQUALS_OP).toString()
        )
    }

    @Test
    fun `test convert less-than CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] < IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.LESS_THAN_OP).toString()
        )
    }

    @Test
    fun `test convert less-or-equal CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] <= IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.LESS_EQUAL_OP).toString()
        )
    }

    @Test
    fun `test convert greater-than CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] > IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.GREATER_THAN_OP).toString()
        )
    }

    @Test
    fun `test convert greater-equals CompareNode to string`() {
        assertEquals(
            "CompareNode[ IntLiteralNode[ 1 ] >= IntLiteralNode[ 2 ] ]",
            CompareNode(IntLiteralNode(1), IntLiteralNode(2), TokenDescriptor.GREATER_EQUALS_OP).toString()
        )
    }

    @Test
    fun `test convert NegationNode to string`() {
        assertEquals(
            "NegationNode[ BoolLiteralNode[ true ] ]",
            NegationNode(BoolLiteralNode(true)).toString()
        )
    }

    //ColonOperatorNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert HeadOperatorNode to string`() {
        assertEquals(
            "HeadOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ]",
            HeadOperatorNode(IntListLiteralNode(listOf(1, 2, 3))).toString()
        )
    }

    @Test
    fun `test convert TailOperatorNode to string`() {
        assertEquals(
            "TailOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ]",
            TailOperatorNode(IntListLiteralNode(listOf(1, 2, 3))).toString()
        )
    }

    @Test
    fun `test convert InitOperatorNode to string`() {
        assertEquals(
            "InitOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ]",
            InitOperatorNode(IntListLiteralNode(listOf(1, 2, 3))).toString()
        )
    }

    @Test
    fun `test convert LastOperatorNode to string`() {
        assertEquals(
            "LastOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ]",
            LastOperatorNode(IntListLiteralNode(listOf(1, 2, 3))).toString()
        )
    }

    @Test
    fun `test convert LenOperatorNode to string`() {
        assertEquals(
            "LenOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ]",
            LenOperatorNode(IntListLiteralNode(listOf(1, 2, 3))).toString()
        )
    }

    //CommandNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert InputCommandNode to string`() {
        assertEquals(
            "InputCommandNode[ number, INT ]",
            InputCommandNode("number", TypeName.INT).toString()
        )
    }

    @Test
    fun `test convert OutputCommandNode to string`() {
        assertEquals(
            "OutputCommandNode[ IntLiteralNode[ 8 ] ]",
            OutputCommandNode(IntLiteralNode(8)).toString()
        )
    }

    //DefinitionNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert ConstantDefNode to string`() {
        assertEquals(
            "ConstantDefNode[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ]",
            ConstantDefNode("PI", TypeName.DOUBLE, DoubleLiteralNode(3.14)).toString()
        )
    }

    @Test
    fun `test convert FunctionDefNode to string`() {
        assertEquals(
            "FunctionDefNode[ PI, INT, [ RuntimeParamDef[ a, INT ], RuntimeParamDef[ b, INT ] ], IntLiteralNode[ 1 ] ]",
            FunctionDefNode(
                "PI",
                TypeName.INT,
                listOf(RuntimeParamDef("a", TypeName.INT, Scope.LOCAL), RuntimeParamDef("b", TypeName.INT, Scope.LOCAL)),
                IntLiteralNode(1)
            ).toString()
        )
    }

    //StatementNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert IfStatementNode to string`() {
        assertEquals(
            "IfStatementNode[ BoolLiteralNode[ true ], IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ]",
            IfStatementNode(BoolLiteralNode(true), IntLiteralNode(1), IntLiteralNode(2)).toString()
        )
    }

    @Test
    fun `test convert FunctionCallNode to string`() {
        assertEquals(
            "FunctionCallNode[ foo, [ IntLiteralNode[ 1 ], DoubleLiteralNode[ 2.0 ], BoolLiteralNode[ true ] ] ]",
            FunctionCallNode(
                FunctionDef("foo", TypeName.INT,
                    listOf(
                        RuntimeParamDef("a", TypeName.INT, Scope.LOCAL),
                        RuntimeParamDef("b", TypeName.DOUBLE, Scope.LOCAL),
                        RuntimeParamDef("c", TypeName.BOOL, Scope.LOCAL))
                    ),
                listOf(IntLiteralNode(1), DoubleLiteralNode(2.0), BoolLiteralNode(true))
            ).toString()
        )
    }

    //ValueReadNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test convert ConstantReadNode to string`() {
        assertEquals(
            "ConstantReadNode[ ConstantDef[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ] ]",
            ConstantReadNode(ConstantDef("PI", TypeName.DOUBLE, DoubleLiteralNode(3.14))).toString()
        )
    }

    @Test
    fun `test convert ParamReadNode to string`() {
        assertEquals(
            "RuntimeParamReadNode[ RuntimeParamDef[ a, INT ] ]",
            RuntimeParamReadNode(RuntimeParamDef("a", TypeName.INT, Scope.LOCAL)).toString()
        )
    }

    //BaseNodes.kt
    @Test
    fun `test convert ProgramNode to string`() {
        assertEquals(
            "ProgramNode" +
            "[ " +
                "ConstantDefNode[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ], " +
                "FunctionDefNode[ foo, INT, [ RuntimeParamDef[ a, INT ], RuntimeParamDef[ b, INT ] ], IntLiteralNode[ 8 ] ] " +
            "]",
            ProgramNode(listOf(
                ConstantDefNode("PI", TypeName.DOUBLE, DoubleLiteralNode(3.14)),
                FunctionDefNode(
                    "foo",
                    TypeName.INT,
                    listOf(RuntimeParamDef("a", TypeName.INT, Scope.LOCAL), RuntimeParamDef("b", TypeName.INT, Scope.LOCAL)),
                    IntLiteralNode(8))
            )).toString()
        )
    }
}
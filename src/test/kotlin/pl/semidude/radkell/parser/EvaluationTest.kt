package pl.semidude.radkell.parser

import org.apache.commons.io.IOUtils
import org.junit.Test
import pl.semidude.radkell.exception.RadkellRuntimeException
import kotlin.test.assertEquals
import pl.semidude.radkell.helpers.*
import kotlin.test.assertFailsWith

class EvaluationTest {

    //LiteralNodes.kt
    //-------------------------------------------------------
    @Test
    fun `test evaluate IntLiteral`() {
        assertEquals(
            8,
            makeParser("output 8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate DoubleLiteralNode`() {
        assertEquals(
            8.0,
            makeParser("output 8.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate BoolLiteralNode`() {
        assertEquals(
            true,
            makeParser("output true;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate IntListLiteralNode`() {
        assertEquals(
            listOf(1, 2, 3),
            makeParser("output [1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate DoubleListLiteralNode`() {
        assertEquals(
            listOf(1.0, 2.0, 3.0),
            makeParser("output [1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate BoolListLiteralNode`() {
        assertEquals(
            listOf(true, false, true),
            makeParser("output [true, false, true];").parse().typeCheck().runProgram().first()
        )
    }

    //ArithmeticNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test evaluate AddNode ints`() {
        assertEquals(
            10,
            makeParser("output 2 + 8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate AddNode doubles`() {
        assertEquals(
            10.0,
            makeParser("output 2.0 + 8.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate SubtractNode ints`() {
        assertEquals(
            -6,
            makeParser("output 2 - 8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate SubtractNode doubles`() {
        assertEquals(
            -6.0,
            makeParser("output 2.0 - 8.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate MultiplyNode ints`() {
        assertEquals(
            16,
            makeParser("output 2 * 8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate MultiplyNode doubles`() {
        assertEquals(
            16.0,
            makeParser("output 2.0 * 8.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate DivideNote ints`() {
        assertEquals(
            0,
            makeParser("output 2 / 8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate DivideNode doubles`() {
        assertEquals(
            0.25,
            makeParser("output 2.0 / 8.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `when division by zero then error`() {
        val parser = makeParser("output 1 / 0;")
        assertFailsWith<RadkellRuntimeException> { parser.parse().typeCheck().runProgram() }
    }

    @Test
    fun `test evaluate UnaryMinusNode int`() {
        assertEquals(
            -8,
            makeParser("output -8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate UnaryMinusNode double`() {
        assertEquals(
            -8.0,
            makeParser("output -8.0;").parse().typeCheck().runProgram().first()
        )
    }

    //LogicalNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test evaluate logical OR`() {
        assertEquals(
            true,
            makeParser("output true || false;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate logical AND`() {
        assertEquals(
            false,
            makeParser("output true && false;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate equal`() {
        assertEquals(
            false,
            makeParser("output 1 == 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate not-equal`() {
        assertEquals(
            true,
            makeParser("output 1 != 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate less-than`() {
        assertEquals(
            true,
            makeParser("output 1 < 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate less-or-equal`() {
        assertEquals(
            true,
            makeParser("output 1 <= 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate greater-than`() {
        assertEquals(
            false,
            makeParser("output 1 > 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate grater-or-equal`() {
        assertEquals(
            false,
            makeParser("output 1 >= 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate comparision of int and double`() {
        assertEquals(
            true,
            makeParser("output 1 == 1.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate comparision of double and int`() {
        assertEquals(
            true,
            makeParser("output 1.0 == 1;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate negation`() {
        assertEquals(
            false,
            makeParser("output !true;").parse().typeCheck().runProgram().first()
        )
    }

    //ColonOperatorNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test evaluate head operator int`() {
        assertEquals(
            1,
            makeParser("output head:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate head operator double`() {
        assertEquals(
            1.0,
            makeParser("output head:[1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate head operator boolean`() {
        assertEquals(
            true,
            makeParser("output head:[true, false, false];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate tail operator int`() {
        assertEquals(
            listOf(2, 3),
            makeParser("output tail:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate tail operator double`() {
        assertEquals(
            listOf(2.0, 3.0),
            makeParser("output tail:[1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate tail operator boolean`() {
        assertEquals(
            listOf(false, false),
            makeParser("output tail:[true, false, false];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate init operator int`() {
        assertEquals(
            listOf(1, 2),
            makeParser("output init:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate init operator double`() {
        assertEquals(
            listOf(1.0, 2.0),
            makeParser("output init:[1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate init operator boolean`() {
        assertEquals(
            listOf(true, false),
            makeParser("output init:[true, false, false];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate last operator int`() {
        assertEquals(
            3,
            makeParser("output last:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate last operator double`() {
        assertEquals(
            3.0,
            makeParser("output last:[1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate last operator boolean`() {
        assertEquals(
            false,
            makeParser("output last:[true, false, false];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate len operator int`() {
        assertEquals(
            3,
            makeParser("output len:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate len operator double`() {
        assertEquals(
            3,
            makeParser("output len:[1.0, 2.0, 3.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate len operator boolean`() {
        assertEquals(
            3,
            makeParser("output len:[true, false, false];").parse().typeCheck().runProgram().first()
        )
    }

    //StatementNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test evaluate if statement positive branch int`() {
        assertEquals(
            1,
            makeParser("output if true then 1 else 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch int`() {
        assertEquals(
            2,
            makeParser("output if false then 1 else 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement positive branch double`() {
        assertEquals(
            1.0,
            makeParser("output if true then 1.0 else 2.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch double`() {
        assertEquals(
            2.0,
            makeParser("output if false then 1.0 else 2.0;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement positive branch boolean`() {
        assertEquals(
            true,
            makeParser("output if true then true else false;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch boolean`() {
        assertEquals(
            false,
            makeParser("output if false then true else false;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement positive branch int list`() {
        assertEquals(
            listOf(1, 2, 3),
            makeParser("output if true then [1, 2, 3] else [3, 2, 1];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch int list`() {
        assertEquals(
            listOf(3, 2, 1),
            makeParser("output if false then [1, 2, 3] else [3, 2, 1];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement positive branch double list`() {
        assertEquals(
            listOf(1.0, 2.0, 3.0),
            makeParser("output if true then [1.0, 2.0, 3.0] else [3.0, 2.0, 1.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch double list`() {
        assertEquals(
            listOf(3.0, 2.0, 1.0),
            makeParser("output if false then [1.0, 2.0, 3.0] else [3.0, 2.0, 1.0];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement positive branch boolean list`() {
        assertEquals(
            listOf(true, false, false),
            makeParser("output if true then [true, false, false] else [false, false, true];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate if statement negative branch boolean list`() {
        assertEquals(
            listOf(false, false, true),
            makeParser("output if false then [true, false, false] else [false, false, true];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate parameterless function call`() {
        val parser = makeParser("""
            def foo(): Int = 123;
            output foo();
        """.trimIndent())

        assertEquals(
            123,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement int`() {
        val parser = makeParser(
            """
            def add(a: Int, b: Int): Int = a + b;
            output add(2, 8);
        """.trimIndent()
        )

        assertEquals(
            10,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement double`() {
        val parser = makeParser(
            """
            def add(a: Double, b: Double): Double= a + b;
            output add(2.0, 8.0);
        """.trimIndent()
        )

        assertEquals(
            10.0,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement boolean`() {
        val parser = makeParser(
            """
            def add(a: Bool, b: Bool): Bool = a || b;
            output add(true, false);
        """.trimIndent()
        )

        assertEquals(
            true,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement int list`() {
        val parser = makeParser(
            """
            def foo(list: [Int]): [Int] = list;
            output foo([1, 2, 3]);
        """.trimIndent()
        )

        assertEquals(
            listOf(1, 2, 3),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement double list`() {
        val parser = makeParser(
            """
            def foo(list: [Double]): [Double] = list;
            output foo([1.0, 2.0, 3.0]);
        """.trimIndent()
        )

        assertEquals(
            listOf(1.0, 2.0, 3.0),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate function call statement boolean list`() {
        val parser = makeParser(
            """
            def foo(list: [Bool]): [Bool] = list;
            output foo([true, false, true]);
        """.trimIndent()
        )

        assertEquals(
            listOf(true, false, true),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    //ValueReadNodes.kt
    //---------------------------------------------------------
    @Test
    fun `test evaluate read constant int`() {
        val parser = makeParser(
            """
            def A: Int = 8;
            output A;
        """.trimIndent()
        )

        assertEquals(
            8,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read constant double`() {
        val parser = makeParser(
            """
            def A: Double = 8.0;
            output A;
        """.trimIndent()
        )

        assertEquals(
            8.0,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read constant boolean`() {
        val parser = makeParser(
            """
            def A: Bool = true;
            output A;
        """.trimIndent()
        )

        assertEquals(
            true,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read constant int list`() {
        val parser = makeParser(
            """
            def A: [Int] = [1, 2, 3];
            output A;
        """.trimIndent()
        )

        assertEquals(
            listOf(1, 2, 3),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read constant double list`() {
        val parser = makeParser(
            """
            def A: [Double] = [1.0, 2.0, 3.0];
            output A;
        """.trimIndent()
        )

        assertEquals(
            listOf(1.0, 2.0, 3.0),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read constant boolean list`() {
        val parser = makeParser(
            """
            def A: [Bool] = [true, false, true];
            output A;
        """.trimIndent()
        )

        assertEquals(
            listOf(true, false, true),
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read parameter int`() {
        val parser = makeParser(
            """
            def foo(number: Int): Int = number;
            output foo(523);
        """.trimIndent()
        )

        assertEquals(
            523,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read parameter double`() {
        val parser = makeParser(
            """
            def foo(number: Double): Double= number;
            output foo(523.0);
        """.trimIndent()
        )

        assertEquals(
            523.0,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test evaluate read parameter boolean`() {
        val parser = makeParser(
            """
            def foo(number: Bool): Bool = number;
            output foo(true);
        """.trimIndent()
        )

        assertEquals(
            true,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    //read parameter for [Int], [Double] and [Bool] tested above (in evaluate function call section)

    @Test
    fun `test evaluate read input slot int`() {
        val parser = makeParser(
            """
            input number as Int;
            output number;
        """.trimIndent()
        )

        assertEquals(
            821,
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("821")).first()
        )
    }

    @Test
    fun `test evaluate read input slot double`() {
        val parser = makeParser(
            """
            input number as Double;
            output number;
        """.trimIndent()
        )

        assertEquals(
            821.0,
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("821.0")).first()
        )
    }

    @Test
    fun `test evaluate read input slot boolean`() {
        val parser = makeParser(
            """
            input number as Bool;
            output number;
        """.trimIndent()
        )

        assertEquals(
            true,
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("true")).first()
        )
    }

    @Test
    fun `test evaluate read input slot int lint`() {
        val parser = makeParser(
            """
            input list as [Int];
            output list;
        """.trimIndent()
        )

        assertEquals(
            listOf(1, 2, 3),
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("1 2 3")).first()
        )
    }

    @Test
    fun `test evaluate read input slot double list`() {
        val parser = makeParser(
            """
            input list as [Double];
            output list;
        """.trimIndent()
        )

        assertEquals(
            listOf(1.0, 2.0, 3.0),
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("1.0 2.0 3.0")).first()
        )
    }

    @Test
    fun `test evaluate read input slot boolean list`() {
        val parser = makeParser(
            """
            input list as [Bool];
            output list;
        """.trimIndent()
        )

        assertEquals(
            listOf(true, false, true),
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("true false true")).first()
        )
    }

    //Operator priority
    //-------------------------------------------------------
    @Test
    fun `test multiplication should have higher priority than addition`() {
        assertEquals(
            6,
            makeParser("output 2 + 2 * 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test division should have higher priority than addition`() {
        assertEquals(
            3,
            makeParser("output 2 + 2 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test parenthesis should have highest priority`() {
        assertEquals(
            8,
            makeParser("output (2 + 2) * 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test equals operator should have lower priority than arithmetic operators`() {
        assertEquals(
            true,
            makeParser("output (2 + 2) * 2 == 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test not-equals operator should have lower priority than arithmetic operators`() {
        assertEquals(
            false,
            makeParser("output (2 + 2) * 2 != 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test less-than operator should have lower priority than arithmetic operators`() {
        assertEquals(
            true,
            makeParser("output (2 + 2) * 2 - 1 < 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test greater-than operator should have lower priority than arithmetic operators`() {
        assertEquals(
            true,
            makeParser("output (2 + 2) * 2 + 1 > 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test less-or-equal operator should have lower priority than arithmetic operators`() {
        assertEquals(
            true,
            makeParser("output (2 + 2) * 2 <= 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test greater-or-equal operator should have lower priority than arithmetic operators`() {
        assertEquals(
            true,
            makeParser("output (2 + 2) * 2 >= 16 / 2;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `colon operator should have higher priority than other operators`() {
        assertEquals(
            7,
            makeParser("output 2 + 2 * 2  + head:[1, 2, 3];").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test unary minus operator used in subtraction expression`() {
        assertEquals(
            0,
            makeParser("output -1 - -1;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test unary minus operator used in multiplication expression`() {
        assertEquals(
            64,
            makeParser("output -8 * -8;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test unary minus operator used before parenthesis`() {
        assertEquals(
            -8,
            makeParser("output -(2 + 6);").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test unary minus operator used twice`() {
        assertEquals(
            1,
            makeParser("output --1;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test unary minus operator used triple`() {
        assertEquals(
            -1,
            makeParser("output ---1;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test negation used before parenthesis`() {
        assertEquals(
            true,
            makeParser("output !(!true || false);").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test negation operator used in complex boolean expression`() {
        assertEquals(
            true,
            makeParser("output !false && (!true || true);").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test negation used twice`() {
        assertEquals(
            true,
            makeParser("output !!true;").parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test negation used triple`() {
        assertEquals(
            false,
            makeParser("output !!!true;").parse().typeCheck().runProgram().first()
        )
    }

    //Complex test examples
    //-------------------------------------------------------

    @Test
    fun `test max function`() {
        val parser = makeParser(
            """
            def max(a: Int, b: Int): Int = if a > b then a else b;
            output max(5, 8);
        """.trimIndent()
        )

        assertEquals(
            8,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test factorial function`() {
        val parser = makeParser(
            """
            def factorial(n: Int): Int =
                if n == 0
                then 1
                else factorial(n-1) * n;

            output factorial(5);
        """.trimIndent()
        )

        assertEquals(
            120,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test sum of squares ranged function`() {
        val parser = makeParser(
            """
            def sumOfSquares(from: Int, to: Int): Int =
                if from == to
                then from*from
                else from*from + sumOfSquares(from + 1, to);

            output sumOfSquares(1, 10);
        """.trimIndent()
        )

        assertEquals(
            385,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test list sum function`() {
        val parser = makeParser(
            """
            def sum(numbers: [Int]): Int =
                if len:numbers == 0
                then 0
                else head:numbers + sum(tail:numbers);

            output sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        """.trimIndent()
        )

        assertEquals(
            55,
            parser.parse().typeCheck().runProgram().first()
        )
    }

    @Test
    fun `test multiple list commands`() {
        val parser = makeParser(
            """
            input int as Int;
            input double as Double;
            input bool as Bool;
            input intList as [Int];
            input doubleList as [Double];
            input booleanList as [Bool];

            output int;
            output double;
            output bool;
            output intList;
            output doubleList;
            output booleanList;
        """.trimIndent()
        )

        assertEquals(
            listOf(
                1,
                1.0,
                true,
                listOf(5, 2, 8),
                listOf(1.0, 2.0),
                listOf(true, false, true)
            ),
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("1 1.0 true 5 2 8 ; 1 2 ; true false true"))
        )
    }
}
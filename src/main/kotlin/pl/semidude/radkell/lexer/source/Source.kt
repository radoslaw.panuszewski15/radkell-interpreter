package pl.semidude.radkell.lexer.source

/**
 * Source abstraction, provides subsequent chars of the underlying source input
 */
interface Source {

    fun hasNext(): Boolean

    fun getNextChar(): Char

    fun peekNextChar(): Char

    fun ungetChar()

    fun getLastCharLineNumber(): Int

    companion object {
        /**
         * End Of Text character
         */
        const val EOT: Char = '$'
    }
}

class EndOfTextException: Exception()
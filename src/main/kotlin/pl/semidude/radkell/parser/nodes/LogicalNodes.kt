package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.TokenDescriptor
import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.exception.RadkellParserException
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing the logical OR expression
 */
class OrNode(
    val exprs: List<ExprNode>,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.BOOL) {

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        exprs.forEach { it.accept(visitor) }
    }

    override fun contentsToString(): String = exprs.joinToString(", ", "[ ", " ]")

    override fun evaluateBool(context: CallContext): Boolean =
        exprs.map { it.evaluateBool(context) }.reduce { acc, b -> acc || b }
}

/**
 * Node representing the logical AND expression
 */
class AndNode(
    val exprs: List<ExprNode>,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.BOOL) {

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        exprs.forEach { it.accept(visitor) }
    }

    override fun contentsToString(): String = exprs.joinToString(", ", "[ ", " ]")

    override fun evaluateBool(context: CallContext): Boolean =
        exprs.map { it.evaluateBool(context) }.reduce { acc, b -> acc && b }
}

/**
 * Node representing a comparision expression
 */
class CompareNode(
    val leftExpr: ExprNode,
    val rightExpr: ExprNode,
    compareDescriptor: TokenDescriptor,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.BOOL) {

    private val compareOp = compareDescriptor.stringValue

    private val compareFunc: (Int) -> Boolean = when(compareDescriptor) {
        TokenDescriptor.LESS_THAN_OP ->      { compareInt -> compareInt < 0 }
        TokenDescriptor.GREATER_THAN_OP ->   { compareInt -> compareInt > 0 }
        TokenDescriptor.LESS_EQUAL_OP ->     { compareInt -> compareInt <= 0 }
        TokenDescriptor.GREATER_EQUALS_OP -> { compareInt -> compareInt >= 0 }
        TokenDescriptor.EQUALS_OP ->         { compareInt -> compareInt == 0 }
        TokenDescriptor.NOT_EQUALS_OP ->     { compareInt -> compareInt != 0 }
        else -> throw RadkellParserException(lineNumber, "internal parser error")
    }

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        leftExpr.accept(visitor)
        rightExpr.accept(visitor)
    }

    override fun contentsToString(): String = "$leftExpr $compareOp $rightExpr"

    override fun evaluateBool(context: CallContext): Boolean =
        compareFunc(leftExpr.compareTo(rightExpr, context))
}

/**
 * Node representing a logical NOT expression
 */
class NegationNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type) {

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        targetExpr.accept(visitor)
    }

    override fun contentsToString(): String = "$targetExpr"

    override fun evaluateBool(context: CallContext): Boolean =
        !targetExpr.evaluateBool(context)
}
package pl.semidude.radkell.cli

import pl.semidude.radkell.helpers.runProgram
import pl.semidude.radkell.helpers.typeCheck
import pl.semidude.radkell.lexer.source.FileSource
import pl.semidude.radkell.lexer.Lexer
import pl.semidude.radkell.exception.RadkellLexerException
import pl.semidude.radkell.lexer.source.Source
import pl.semidude.radkell.lexer.source.StringSource
import pl.semidude.radkell.parser.Parser
import pl.semidude.radkell.exception.RadkellParserException
import pl.semidude.radkell.exception.RadkellRuntimeException

class CLIEntryPoint {

    companion object {
        @JvmStatic fun main(args: Array<String>) {

            if (args.isEmpty()) {
                System.err.println("No source file provided")
                return
            }

            val source: Source = FileSource(args[0])
            val lexer = Lexer(source)
            val parser = Parser(lexer)

            try {
                parser.parse().typeCheck().runProgram(System.`in`)
            }
            catch (e: RadkellLexerException) {
                println(e.message)
            }
            catch (e: RadkellParserException)  {
                println(e.message)
            }
            catch (e: RadkellRuntimeException) {
                println(e.message)
            }
            catch (e: Throwable) {
                println("[INTERPRETER ERROR] $e")
            }
        }
    }
}
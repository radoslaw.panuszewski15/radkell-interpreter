package pl.semidude.radkell.helpers

import pl.semidude.radkell.lexer.Lexer
import pl.semidude.radkell.lexer.source.StringSource
import pl.semidude.radkell.parser.ParseTree
import pl.semidude.radkell.parser.Parser
import pl.semidude.radkell.parser.visitors.RunProgramVisitor
import pl.semidude.radkell.parser.visitors.TypeCheckVisitor
import java.io.InputStream

fun makeParser(input: String): Parser {
    val source = StringSource(input)
    val lexer = Lexer(source)
    val parser = Parser(lexer)
    return parser
}

fun ParseTree.typeCheck(): ParseTree {
    accept(TypeCheckVisitor())
    return this
}

fun ParseTree.runProgram(inputStream: InputStream = System.`in`): List<Any> {
    val visitor = RunProgramVisitor(inputStream)
    accept(visitor)
    return visitor.outputResults
}
package pl.semidude.radkell.lexer

import pl.semidude.radkell.exception.RadkellLexerException
import pl.semidude.radkell.lexer.TokenDescriptor.*
import pl.semidude.radkell.lexer.source.Source
import java.lang.StringBuilder
import java.util.*

/**
 * Class which performs lexical analysis of the source code and produces tokens
 */
class Lexer(private var source: Source) {

    private val tokenMap: Map<String, TokenDescriptor> = values().associateBy { it.stringValue }
    private var lastToken: Token? = null
    private val previousTokens: Deque<Token> = LinkedList()
    private val waitingTokens: Deque<Token> = LinkedList()

    fun hasNext(): Boolean = lastToken?.descriptor != END

    fun getLastTokenLineNumber() = source.getLastCharLineNumber()

    fun ungetToken(): Token? {
        if (previousTokens.isNotEmpty())
            waitingTokens.push(previousTokens.pop())
        return previousTokens.peekFirst()
    }

    fun getNextToken(): Token? {
        if (waitingTokens.isNotEmpty())
            return waitingTokens.pop().also { previousTokens.push(it) }

        if (!source.hasNext())
            return null

        val char = ignoreWhitespaces()

        lastToken = when {
            char.isLetter() -> processLetter(char)
            char.isDigit() -> processDigit(char)
            char.isEndOfText() -> Token(END)
            else -> processSpecialCharacter(char)
        }
        return lastToken.also { previousTokens.push(it) }
    }

    private fun ignoreWhitespaces(): Char {
        var char = source.getNextChar()
        while (char.isWhitespace()) char = source.getNextChar()
        return char
    }

    //==========================================================================================================

    private fun processLetter(startChar: Char): Token {
        with(StringBuilder()) {
            tryExtractIdentifier(startChar, onKeywordEarly = { return it })
            checkIfColonOperator(onColonOperator = { return it })
            checkIfBoolLiteral(onBoolLiteral = { return it })
            return Identifier(toString())
        }
    }

    private inline fun StringBuilder.tryExtractIdentifier(startChar: Char, onKeywordEarly: (Token) -> Unit) {
        var char = startChar
        do {
            append(char)
            if (isSimpleToken() && !source.peekNextChar().isLetterOrDigit()) onKeywordEarly(makeSimpleToken())
            char = source.getNextChar()
        } while (char.isLetter() || char.isDigit())
        source.ungetChar()
    }

    private inline fun StringBuilder.checkIfColonOperator(onColonOperator: (Token) -> Unit) {
        if (source.getNextChar() == ':') {
            append(':')
            if (isSimpleToken()) onColonOperator(makeSimpleToken())
            else deleteLast()
        }
        source.ungetChar()
    }

    private inline fun StringBuilder.checkIfBoolLiteral(onBoolLiteral: (Token) -> Unit) {
        if (toString() == "true" || toString() == "false")
            onBoolLiteral(BoolLiteral(toString().toBoolean()))
    }

    //==========================================================================================================

    private fun processDigit(startChar: Char): Token {
        with(StringBuilder()) {
            tryExtractNumber(startChar)
            tryExtractFractionalPart(onDoubleLiteral = { return it })
            return IntLiteral(toString().toInt())
        }
    }

    private fun StringBuilder.tryExtractNumber(startChar: Char) {
        var char = startChar
        while (char.isDigit()) {
            append(char)
            char = source.getNextChar()
        }
        if (char.isLetter()) throw RadkellLexerException(
            source.getLastCharLineNumber(),
            "number literal cannot contain or end with a letter"
        )
        else source.ungetChar()
    }

    private inline fun StringBuilder.tryExtractFractionalPart(onDoubleLiteral: (Token) -> Unit) {
        if (source.getNextChar() == '.') {
            append('.')
            tryExtractNumber(source.getNextChar())
            onDoubleLiteral(DoubleLiteral(toString().toDouble()))
        }
        source.ungetChar()
    }

    //==========================================================================================================

    private fun processSpecialCharacter(startChar: Char): Token {
        checkIfTwoCharacterOperator(startChar, onTwoCharacterOperator = { return it })
        checkIfListType(startChar, onListType = { return it })
        checkIfSimpleToken(startChar, onSimpleToken = { return it })
        throw RadkellLexerException(
            source.getLastCharLineNumber(),
            "invalid operator '$startChar'"
        )
    }

    private inline fun checkIfTwoCharacterOperator(startChar: Char, onTwoCharacterOperator: (Token) -> Unit) {
        when (startChar) {
            '=' -> {
                if (source.getNextChar() == '=') onTwoCharacterOperator(Token(EQUALS_OP))
                else source.ungetChar()
            }
            '!' -> {
                if (source.getNextChar() == '=') onTwoCharacterOperator(Token(NOT_EQUALS_OP))
                else source.ungetChar()
            }
            '<' -> {
                if (source.getNextChar() == '=') onTwoCharacterOperator(Token(LESS_EQUAL_OP))
                else source.ungetChar()
            }
            '>' -> {
                if (source.getNextChar() == '=') onTwoCharacterOperator(Token(GREATER_EQUALS_OP))
                else source.ungetChar()
            }
            '|' -> {
                if (source.getNextChar() == '|') onTwoCharacterOperator(Token(OR_OP))
                else source.ungetChar()
            }
            '&' -> {
                if (source.getNextChar() == '&') onTwoCharacterOperator(Token(AND_OP))
                else source.ungetChar()
            }
        }
    }

    private inline fun checkIfListType(startChar: Char, onListType: (Token) -> Unit) {
        if (startChar == '[' && !source.peekNextChar().isEndOfText()) {
            with(StringBuilder()) {
                append('[')
                tryExtractIdentifier(source.getNextChar(), onKeywordEarly = { /*do nothing*/ })
                if (source.getNextChar() == ']')
                    append(']')

                if (isSimpleToken()) onListType(makeSimpleToken())
                else {
                    while (source.peekNextChar() != '[') {
                        source.ungetChar()
                    }
                    source.getNextChar()
                }
            }
        }
    }

    private inline fun checkIfSimpleToken(char: Char, onSimpleToken: (Token) -> Unit) {
        if (isSimpleToken(char)) onSimpleToken(makeSimpleToken(char))
    }

    //==========================================================================================================

    private fun makeSimpleToken(startChar: Char) = Token(tokenMap.getValue(startChar.toString()))

    private fun isSimpleToken(char: Char) = tokenMap.containsKey(char.toString())

    private fun StringBuilder.isSimpleToken() = tokenMap.containsKey(this.toString())

    private fun StringBuilder.makeSimpleToken() = Token(tokenMap.getValue(this.toString()))

    private fun StringBuilder.deleteLast() = deleteCharAt(length - 1)

    private fun Char.isEndOfText() = this == Source.EOT
}
package pl.semidude.radkell.parser.visitors

import pl.semidude.radkell.exception.RadkellParserException
import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.*
import pl.semidude.radkell.parser.nodes.*

/**
 * Visitor responsible for performing the type checks
 */
class TypeCheckVisitor : ParseTreeVisitor {

    override fun visit(parseTree: ParseTree) {
        //no action needed
    }

    override fun visit(node: ProgramNode) {
        //no check needed
    }

    override fun visit(node: InputCommandNode) {
        //no check needed
    }

    override fun visit(node: OutputCommandNode) {
        //no check needed
    }

    override fun visit(node: ConstantDefNode) {
        if (node.initializerExpr.type != node.type)
            throw RadkellParserException(
                node.lineNumber,
                "could not assign ${node.initializerExpr.type} to ${node.type}"
            )
    }

    override fun visit(node: FunctionDefNode) {
        if (node.returnType != node.bodyExpr.type)
            throw RadkellParserException(
                node.lineNumber, "function '${node.name}' inferred body type is ${node.bodyExpr.type}, " +
                        "but decalred return type is ${node.returnType}"
            )
    }

    override fun visit(node: HeadOperatorNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT_LIST, TypeName.DOUBLE_LIST, TypeName.BOOL_LIST))
            throw RadkellParserException(
                node.lineNumber,
                "could not apply head operator to non-list value"
            )
    }

    override fun visit(node: TailOperatorNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT_LIST, TypeName.DOUBLE_LIST, TypeName.BOOL_LIST))
            throw RadkellParserException(
                node.lineNumber,
                "could not apply tail operator to non-list value"
            )
    }

    override fun visit(node: InitOperatorNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT_LIST, TypeName.DOUBLE_LIST, TypeName.BOOL_LIST))
            throw RadkellParserException(
                node.lineNumber,
                "could not apply init operator to non-list value"
            )
    }

    override fun visit(node: LastOperatorNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT_LIST, TypeName.DOUBLE_LIST, TypeName.BOOL_LIST))
            throw RadkellParserException(
                node.lineNumber,
                "could not apply last operator to non-list value"
            )
    }

    override fun visit(node: LenOperatorNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT_LIST, TypeName.DOUBLE_LIST, TypeName.BOOL_LIST))
            throw RadkellParserException(
                node.lineNumber,
                "could not apply len operator to non-list value"
            )
    }

    override fun visit(node: OrNode) {
        if (node.exprs.any { it.type != TypeName.BOOL })
            throw RadkellParserException(
                node.lineNumber,
                "operands of AND operator should all have BOOL types, but given ${node.exprs.map { it.type }}"
            )
    }

    override fun visit(node: AndNode) {
        if (node.exprs.any { it.type != TypeName.BOOL })
            throw RadkellParserException(
                node.lineNumber,
                "operands of AND operator should all have BOOL types, but given ${node.exprs.map { it.type }}"
            )
    }

    override fun visit(node: CompareNode) {
        if (node.leftExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE) ||
            node.rightExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE)
        )
            throw RadkellParserException(
                node.lineNumber,
                "cannot compare ${node.leftExpr.type} and ${node.rightExpr.type}"
            )
    }

    override fun visit(node: NegationNode) {
        if (node.targetExpr.type != TypeName.BOOL)
            throw RadkellParserException(
                node.lineNumber,
                "negation operator is not applicable to type ${node.targetExpr.type}"
            )
    }

    override fun visit(node: AddNode) {
        if (node.leftExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE) ||
            node.rightExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE)
        )
            throw RadkellParserException(
                node.lineNumber,
                "could not add operands of types ${node.leftExpr.type} and ${node.rightExpr.type}"
            )
    }

    override fun visit(node: SubtractNode) {
        if (node.leftExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE) ||
            node.rightExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE)
        )
            throw RadkellParserException(
                node.lineNumber,
                "could not subtract operands of types ${node.leftExpr.type} and ${node.rightExpr.type}"
            )
    }

    override fun visit(node: MultiplyNode) {
        if (node.leftExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE) ||
            node.rightExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE)
        )
            throw RadkellParserException(
                node.lineNumber,
                "could not multiply operands of types ${node.leftExpr.type} and ${node.rightExpr.type}"
            )
    }

    override fun visit(node: DivideNode) {
        if (node.leftExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE) ||
            node.rightExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE)
        )
            throw RadkellParserException(
                node.lineNumber,
                "could not divide operands of types ${node.leftExpr.type} and ${node.rightExpr.type}"
            )
    }

    override fun visit(node: UnaryMinusNode) {
        if (node.targetExpr.type !in listOf(TypeName.INT, TypeName.DOUBLE))
            throw RadkellParserException(
                node.lineNumber,
                "unary minus operator is not applicable to type ${node.targetExpr.type}"
            )
    }

    override fun visit(node: IntLiteralNode) {
        //no check needed
    }

    override fun visit(node: DoubleLiteralNode) {
        //no check needed
    }

    override fun visit(node: BoolLiteralNode) {
        //no check needed
    }

    override fun visit(node: IntListLiteralNode) {
        //no check needed
    }

    override fun visit(node: DoubleListLiteralNode) {
        //no check needed
    }

    override fun visit(node: BoolListLiteralNode) {
        //no check needed
    }

    override fun visit(node: IfStatementNode) {
        if (node.condExpr.type != TypeName.BOOL)
            throw RadkellParserException(
                node.lineNumber,
                "if condition should be of type Bool, but given ${node.condExpr.type}"
            )

        if (node.thenExpr.type != node.elseExpr.type)
            throw RadkellParserException(
                node.lineNumber, "if branches should have the same types, " +
                        "but given ${node.thenExpr.type} and ${node.elseExpr.type}"
            )
    }

    override fun visit(node: ConstantReadNode) {
        //no check needed
    }

    override fun visit(node: RuntimeParamReadNode) {
        //no check needed
    }

    override fun visit(node: FunctionCallNode) {
        for ((i, paramDef) in node.functionDef.params.withIndex()) {
            if (paramDef.type != node.paramValues[i].type)
                throw RadkellParserException(
                    node.lineNumber, "in call function '${node.functionDef.name}' parameter types don't match " +
                            "(for parameter ${i + 1} provided ${node.paramValues[i].type} but expected ${paramDef.type})"
                )
        }
    }
}
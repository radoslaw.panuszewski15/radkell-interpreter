package pl.semidude.radkell.lexer

import org.junit.Test
import pl.semidude.radkell.exception.RadkellLexerException
import pl.semidude.radkell.lexer.source.StringSource
import kotlin.test.*

fun makeLexer(input: String): Lexer {
    val source = StringSource(input)
    return Lexer(source)
}

class LexerTest {

    @Test
    fun `test scan identifier`() {
        assertEquals(
            "[IDENTIFIER] PI",
            makeLexer("PI").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan int literal`() {
        assertEquals(
            "[INT_LITERAL] 123",
            makeLexer("123").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan double literal`() {
        assertEquals(
            "[DOUBLE_LITERAL] 123.0",
            makeLexer("123.0").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan bool literal`() {
        assertEquals(
            "[BOOL_LITERAL] true",
            makeLexer("true").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan def keyword`() {
        assertEquals(
            "[DEF_KW]",
            makeLexer("def").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan if keyword`() {
        assertEquals(
            "[IF_KW]",
            makeLexer("if").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan then keyword`() {
        assertEquals(
            "[THEN_KW]",
            makeLexer("then").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan else keyword`() {
        assertEquals(
            "[ELSE_KW]",
            makeLexer("else").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan output keyword`() {
        assertEquals(
            "[OUTPUT_KW]",
            makeLexer("output").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan input keyword`() {
        assertEquals(
            "[INPUT_KW]",
            makeLexer("input").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan as keyword`() {
        assertEquals(
            "[AS_KW]",
            makeLexer("as").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan colon`() {
        assertEquals(
            "[COLON]",
            makeLexer(":").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan semicolon`() {
        assertEquals(
            "[SEMICOLON]",
            makeLexer(";").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan comma`() {
        assertEquals(
            "[COMMA]",
            makeLexer(",").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan dot`() {
        assertEquals(
            "[DOT]",
            makeLexer(".").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan open parenthesis`() {
        assertEquals(
            "[OPEN_PARENTHESIS]",
            makeLexer("(").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan close parenthesis`() {
        assertEquals(
            "[CLOSE_PARENTHESIS]",
            makeLexer(")").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan open square bracket`() {
        assertEquals(
            "[OPEN_SQUARE_BRACKET]",
            makeLexer("[").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan close square bracket`() {
        assertEquals(
            "[CLOSE_SQUARE_BRACKET]",
            makeLexer("]").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan int type`() {
        assertEquals(
            "[INT_TYPE]",
            makeLexer("Int").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan double type`() {
        assertEquals(
            "[DOUBLE_TYPE]",
            makeLexer("Double").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan bool type`() {
        assertEquals(
            "[BOOL_TYPE]",
            makeLexer("Bool").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan int list type`() {
        assertEquals(
            "[INT_LIST_TYPE]",
            makeLexer("[Int]").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan double list type`() {
        assertEquals(
            "[DOUBLE_LIST_TYPE]",
            makeLexer("[Double]").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan bool list type`() {
        assertEquals(
            "[BOOL_LIST_TYPE]",
            makeLexer("[Bool]").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan assign operator`() {
        assertEquals(
            "[ASSIGN_OP]",
            makeLexer("=").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan less-then operator`() {
        assertEquals(
            "[LESS_THAN_OP]",
            makeLexer("<").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan grater-then operator`() {
        assertEquals(
            "[GREATER_THAN_OP]",
            makeLexer(">").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan less-or-equal operator`() {
        assertEquals(
            "[LESS_EQUAL_OP]",
            makeLexer("<=").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan greater-or-equal operator`() {
        assertEquals(
            "[GREATER_EQUALS_OP]",
            makeLexer(">=").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan equals operator`() {
        assertEquals(
            "[EQUALS_OP]",
            makeLexer("==").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan not-equals operator`() {
        assertEquals(
            "[NOT_EQUALS_OP]",
            makeLexer("!=").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan add operator`() {
        assertEquals(
            "[ADD_OP]",
            makeLexer("+").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan subtract operator`() {
        assertEquals(
            "[SUBTRACT_OP]",
            makeLexer("-").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan multiply operator`() {
        assertEquals(
            "[MULTIPLY_OP]",
            makeLexer("*").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan divide operator`() {
        assertEquals(
            "[DIVIDE_OP]",
            makeLexer("/").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan logical or operator`() {
        assertEquals(
            "[OR_OP]",
            makeLexer("||").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan logical AND operator`() {
        assertEquals(
            "[AND_OP]",
            makeLexer("&&").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan not operator`() {
        assertEquals(
            "[NOT_OP]",
            makeLexer("!").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan head operator`() {
        assertEquals(
            "[HEAD_OP]",
            makeLexer("head:").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan tail operator`() {
        assertEquals(
            "[TAIL_OP]",
            makeLexer("tail:").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan init operator`() {
        assertEquals(
            "[INIT_OP]",
            makeLexer("init:").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan last operator`() {
        assertEquals(
            "[LAST_OP]",
            makeLexer("last:").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan len operator`() {
        assertEquals(
            "[LEN_OP]",
            makeLexer("len:").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test scan end of text`() {
        assertEquals(
            "[END]",
            makeLexer("$").getNextToken()!!.toString()
        )
    }

    @Test
    fun `test define double constant`() {
        val lexer = makeLexer("def PI: Double = 3.14;")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] PI
            [COLON]
            [DOUBLE_TYPE]
            [ASSIGN_OP]
            [DOUBLE_LITERAL] 3.14
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test define negative int constant`() {
        val lexer = makeLexer("def NEGATIVE: Int = -1;")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] NEGATIVE
            [COLON]
            [INT_TYPE]
            [ASSIGN_OP]
            [SUBTRACT_OP]
            [INT_LITERAL] 1
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test invalid operator @`() {
        val lexer = makeLexer("def PI: Double @ 3.14;")
        assertFailsWith<RadkellLexerException> { generateSequence { lexer.getNextToken() }.joinToString("\n") }
    }

    @Test
    fun `test invalid operator ^`() {
        val lexer = makeLexer("def PI: Double = 3.14^2;")
        assertFailsWith<RadkellLexerException> { generateSequence { lexer.getNextToken() }.joinToString("\n") }
    }

    @Test
    fun `test invalid double constant 1`() {
        val lexer = makeLexer("def PI: Double = 3a.14;")
        assertFailsWith<RadkellLexerException> { generateSequence { lexer.getNextToken() }.joinToString("\n") }
    }

    @Test
    fun `test invalid double constant 2`() {
        val lexer = makeLexer("def PI: Double = 3.14a;")
        assertFailsWith<RadkellLexerException> { generateSequence { lexer.getNextToken() }.joinToString("\n") }
    }

    @Test
    fun `test define bool constant`() {
        val lexer = makeLexer("def amIAwesome: Bool = true;")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] amIAwesome
            [COLON]
            [BOOL_TYPE]
            [ASSIGN_OP]
            [BOOL_LITERAL] true
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test define simple function`() {
        val lexer = makeLexer("def circleField(r: Double): Double = PI*r*r;")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] circleField
            [OPEN_PARENTHESIS]
            [IDENTIFIER] r
            [COLON]
            [DOUBLE_TYPE]
            [CLOSE_PARENTHESIS]
            [COLON]
            [DOUBLE_TYPE]
            [ASSIGN_OP]
            [IDENTIFIER] PI
            [MULTIPLY_OP]
            [IDENTIFIER] r
            [MULTIPLY_OP]
            [IDENTIFIER] r
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test define recursive function`() {
        val lexer = makeLexer("""
            def factorial(n: Int): Int =
                if n == 0
                then 1
                else factorial(n-1) * n;
        """.trimIndent())

        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] factorial
            [OPEN_PARENTHESIS]
            [IDENTIFIER] n
            [COLON]
            [INT_TYPE]
            [CLOSE_PARENTHESIS]
            [COLON]
            [INT_TYPE]
            [ASSIGN_OP]
            [IF_KW]
            [IDENTIFIER] n
            [EQUALS_OP]
            [INT_LITERAL] 0
            [THEN_KW]
            [INT_LITERAL] 1
            [ELSE_KW]
            [IDENTIFIER] factorial
            [OPEN_PARENTHESIS]
            [IDENTIFIER] n
            [SUBTRACT_OP]
            [INT_LITERAL] 1
            [CLOSE_PARENTHESIS]
            [MULTIPLY_OP]
            [IDENTIFIER] n
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test define list recursive function`() {
        val lexer = makeLexer("""
            def sum(numbers: [Double]): Double =
                head:numbers + sum(tail:numbers);
        """.trimIndent())

        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] sum
            [OPEN_PARENTHESIS]
            [IDENTIFIER] numbers
            [COLON]
            [DOUBLE_LIST_TYPE]
            [CLOSE_PARENTHESIS]
            [COLON]
            [DOUBLE_TYPE]
            [ASSIGN_OP]
            [HEAD_OP]
            [IDENTIFIER] numbers
            [ADD_OP]
            [IDENTIFIER] sum
            [OPEN_PARENTHESIS]
            [TAIL_OP]
            [IDENTIFIER] numbers
            [CLOSE_PARENTHESIS]
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test multiple commands`() {
        val lexer = makeLexer("""
            input number as Int;
            output factorial(number);
            output circleField(15);
            output sum([1, 2, 3, 4, 5]);
        """.trimIndent())

        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [INPUT_KW]
            [IDENTIFIER] number
            [AS_KW]
            [INT_TYPE]
            [SEMICOLON]
            [OUTPUT_KW]
            [IDENTIFIER] factorial
            [OPEN_PARENTHESIS]
            [IDENTIFIER] number
            [CLOSE_PARENTHESIS]
            [SEMICOLON]
            [OUTPUT_KW]
            [IDENTIFIER] circleField
            [OPEN_PARENTHESIS]
            [INT_LITERAL] 15
            [CLOSE_PARENTHESIS]
            [SEMICOLON]
            [OUTPUT_KW]
            [IDENTIFIER] sum
            [OPEN_PARENTHESIS]
            [OPEN_SQUARE_BRACKET]
            [INT_LITERAL] 1
            [COMMA]
            [INT_LITERAL] 2
            [COMMA]
            [INT_LITERAL] 3
            [COMMA]
            [INT_LITERAL] 4
            [COMMA]
            [INT_LITERAL] 5
            [CLOSE_SQUARE_BRACKET]
            [CLOSE_PARENTHESIS]
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test identifier beginning with keyword`() {
        val lexer = makeLexer("def defense: Bool1 = trueue")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] defense
            [COLON]
            [IDENTIFIER] Bool1
            [ASSIGN_OP]
            [IDENTIFIER] trueue
            [END]
        """.trimIndent(), output)
    }

    @Test
    fun `test len operator`() {
        val lexer = makeLexer("def length(list: [Int]): Int = len:list;")
        val output = generateSequence { lexer.getNextToken() }.joinToString("\n")
        assertEquals("""
            [DEF_KW]
            [IDENTIFIER] length
            [OPEN_PARENTHESIS]
            [IDENTIFIER] list
            [COLON]
            [INT_LIST_TYPE]
            [CLOSE_PARENTHESIS]
            [COLON]
            [INT_TYPE]
            [ASSIGN_OP]
            [LEN_OP]
            [IDENTIFIER] list
            [SEMICOLON]
            [END]
        """.trimIndent(), output)
    }
}
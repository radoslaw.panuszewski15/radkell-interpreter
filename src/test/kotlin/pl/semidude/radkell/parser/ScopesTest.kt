package pl.semidude.radkell.parser

import org.apache.commons.io.IOUtils
import org.junit.Test
import pl.semidude.radkell.exception.RadkellParserException
import kotlin.test.assertFailsWith
import pl.semidude.radkell.helpers.*
import kotlin.test.assertEquals

class ScopesTest {

    @Test
    fun `when access not defined parameter then error`() {
        val parser = makeParser("def foo(a: Int): Int = a + b;")
        assertFailsWith<RadkellParserException> { parser.parse() }
    }

    @Test
    fun `when define list with elements of different types then error`() {
        val parser = makeParser("output [1.0, 2, 3];")
        assertFailsWith<RadkellParserException> { parser.parse() }
    }

    @Test
    fun `when access parameter of another function then error out of scope`() {
        val parser = makeParser(
            """
            def square(n: Int): Int = n*n;
            def add(a: Int, b: Int): Int = a + b + n;
        """.trimIndent()
        )
        assertFailsWith<RadkellParserException> { parser.parse() }
    }

    @Test
    fun `test enter and leave function scope`() {
        val parser = makeParser(
            """
            def foo(a: Int): Int = a;
            input number as Int;
            output number;
        """.trimIndent()
        )

        assertEquals(
            821,
            parser.parse().typeCheck().runProgram(IOUtils.toInputStream("821")).first()
        )
    }

    @Test
    fun `when define constant then symbol is defined`() {
        makeParser(
            """
            def number: Double = 3.14;
            output number;
        """.trimIndent()
        ).parse().typeCheck()
    }

    @Test
    fun `when define constant and use it with typo then undefined error`() {
        val parser = makeParser(
            """
            def number: Double = 3.14;
            output number1;
        """.trimIndent()
        )
        assertFailsWith<RadkellParserException> { parser.parse() }
    }

    @Test
    fun `when input command then constant is defined`() {
        makeParser(
            """
            input number as Int;
            output number;
        """.trimIndent()
        ).parse().typeCheck()
    }

    @Test
    fun `when define function then function is defined`() {
        makeParser(
            """
            def foo(a: Int): Int = 123;
            output foo(1);
        """.trimIndent()
        ).parse().typeCheck()
    }

    @Test
    fun `global symbols should be accessible in a function`() {
        makeParser(
            """
            def PI: Double = 3.14;
                
            def multiplyByPi(number: Double): Double = number * PI;
            
            output multiplyByPi(2.0);
        """.trimIndent()
        ).parse().typeCheck()
    }

    @Test
    fun `input slots should be accessible in a function`() {
        makeParser(
            """
            input number as Int;
            
            def doubleNumber(): Int = number * 2;
            
            output doubleNumber();
        """.trimIndent()
        ).parse().typeCheck().runProgram(IOUtils.toInputStream("8"))
    }
}
package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing the input command
 */
class InputCommandNode(
    val name: String,
    val type: TypeName,
    override val lineNumber: Int = 0
) : Node() {

    override fun accept(visitor: ParseTreeVisitor) = visitor.visit(this)

    override fun contentsToString(): String = "$name, $type"
}

/**
 * Node representing the output command
 */
class OutputCommandNode(
    val expr: ExprNode,
    override val lineNumber: Int = 0
) : Node() {

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        expr.accept(visitor)
    }

    override fun contentsToString(): String = "$expr"
}
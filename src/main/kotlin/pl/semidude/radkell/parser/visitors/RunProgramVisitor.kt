package pl.semidude.radkell.parser.visitors

import pl.semidude.radkell.helpers.buildList
import pl.semidude.radkell.helpers.emptyMutableList
import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.nodes.InputCommandNode
import pl.semidude.radkell.parser.nodes.OutputCommandNode
import pl.semidude.radkell.parser.ParseTree
import pl.semidude.radkell.exception.RadkellRuntimeException
import pl.semidude.radkell.parser.RuntimeParamDef
import pl.semidude.radkell.parser.Scope
import java.io.InputStream
import java.lang.Exception
import java.lang.NumberFormatException
import java.util.*

/**
 * Visitor responsible for processing commands and thus - actually running the program.
 */
class RunProgramVisitor(inputStream: InputStream = System.`in`) : AbstractParseTreeVisitor() {

    val outputResults = emptyMutableList<Any>()
    private val scanner = Scanner(inputStream).also { it.useLocale(Locale.US) }
    private lateinit var parseTree: ParseTree

    /**
     * Save the [ParseTree] for further use
     */
    override fun visit(parseTree: ParseTree) {
        this.parseTree = parseTree
    }

    /**
     * Start the expression evaluation process, this is likely to process most of the underlying program functionality
     */
    override fun visit(node: OutputCommandNode) {
        val value: Any = node.expr.evaluate(parseTree.globalContext)
        println(value)
        outputResults += value
    }

    /**
     * Load the input slots from given [InputStream]
     */
    override fun visit(node: InputCommandNode) {
        print("Provide value for slot '${node.name}': ")

        try {
            val value: Any = when (node.type) {
                TypeName.INT -> scanner.nextInt()
                TypeName.DOUBLE -> scanner.nextDouble()
                TypeName.BOOL -> scanner.nextBoolean()
                TypeName.INT_LIST -> scanList(String::toInt)
                TypeName.DOUBLE_LIST -> scanList(String::toDouble)
                TypeName.BOOL_LIST -> scanList(String::toBoolean)
            }
            parseTree.globalContext =
                parseTree.globalContext.includeParam(RuntimeParamDef(node.name, node.type, Scope.GLOBAL), value)
        }
        catch (e: NumberFormatException) {
            throw RadkellRuntimeException(0, "Invalid input provided for slot '${node.name}' of type ${node.type}")
        }
        catch (e: InputMismatchException) {
            throw RadkellRuntimeException(0, "Invalid input provided for slot '${node.name}' of type ${node.type}")
        }
        catch (e: Exception) {
            e.printStackTrace()
            throw RadkellRuntimeException(0, e.message ?: "unkwnown error occured")
        }
    }

    private fun <T> scanList(typeConverter: (String) -> T): List<T> {
        return buildList {
            while (scanner.hasNext()) {
                val str = scanner.next()
                if (str == ";") break
                add(typeConverter(str))
            }
        }
    }
}
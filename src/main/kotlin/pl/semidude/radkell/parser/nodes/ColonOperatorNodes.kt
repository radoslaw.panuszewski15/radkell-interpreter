package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.mapToElementType
import pl.semidude.radkell.exception.RadkellRuntimeException
import pl.semidude.radkell.lexer.TypeName.*
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.evaluation.Evaluable
import pl.semidude.radkell.parser.evaluation.ListReducerEvaluator
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing the head: operator
 */
class HeadOperatorNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type.mapToElementType()),

    Evaluable by ListReducerEvaluator(
        evaluable = targetExpr,
        reducer = { list -> list.first() }
    ) {

    override fun accept(visitor: ParseTreeVisitor) {
        targetExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$targetExpr"
}

/**
 * Node representing the tail: operator
 */
class TailOperatorNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type),

    Evaluable by ListReducerEvaluator(
        evaluable = targetExpr,
        reducer = { list -> list.subList(1, list.size) }
    ) {

    override fun accept(visitor: ParseTreeVisitor) {
        targetExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$targetExpr"
}

/**
 * Node representing the init: operator
 */
class InitOperatorNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type),

    Evaluable by ListReducerEvaluator(
        evaluable = targetExpr,
        reducer = { list -> list.subList(0, list.size - 1) }
    ) {

    override fun accept(visitor: ParseTreeVisitor) {
        targetExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$targetExpr"
}

/**
 * Node representing the last: operator
 */
class LastOperatorNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type.mapToElementType()),

    Evaluable by ListReducerEvaluator(
        evaluable = targetExpr,
        reducer = { list -> list.last() }
    ) {

    override fun accept(visitor: ParseTreeVisitor) {
        targetExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$targetExpr"
}

/**
 * Node representing the len: operator
 */
class LenOperatorNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(INT) {

    override fun accept(visitor: ParseTreeVisitor) {
        targetExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$targetExpr"

    override fun evaluateInt(context: CallContext): Int {
        if (targetExpr.type !in listOf(INT_LIST, DOUBLE_LIST, BOOL_LIST))
            throw RadkellRuntimeException(lineNumber, "len operator used on non-list value")

        return (targetExpr.evaluate(context) as List<*>).size
    }
}
package pl.semidude.radkell.parser

import org.junit.Test
import pl.semidude.radkell.exception.RadkellParserException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import pl.semidude.radkell.helpers.*

class TreeGenerationTest {

    //LiteralNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when int literal then IntLiteralNode is generated`() {
        val parseTree = makeParser("output 8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ IntLiteralNode[ 8 ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when double literal then DoubleLiteralNode is generated`() {
        val parseTree = makeParser("output 8.0;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ DoubleLiteralNode[ 8.0 ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when bool literal then BoolLiteralNode is generated`() {
        val parseTree = makeParser("output true;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ BoolLiteralNode[ true ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when int list literal then IntListLiteralNode is generated`() {
        val parseTree = makeParser("output [1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ IntListLiteralNode[ [1, 2, 3] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when double list literal then DoubleListLiteralNode is generated`() {
        val parseTree = makeParser("output [1.0, 2.0, 3.0];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ DoubleListLiteralNode[ [1.0, 2.0, 3.0] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when bool list literal then BoolListLiteralNode is generated`() {
        val parseTree = makeParser("output [true, false, true];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ BoolListLiteralNode[ [true, false, true] ] ] ]",
            parseTree.toString()
        )
    }

    //ArithmeticNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when addition then AddNode is generated`() {
        val parseTree = makeParser("output 2 + 8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ AddNode[ IntLiteralNode[ 2 ], IntLiteralNode[ 8 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when subtraction then SubtractNode is generated`() {
        val parseTree = makeParser("output 2 - 8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ SubtractNode[ IntLiteralNode[ 2 ], IntLiteralNode[ 8 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when multiplication then MultiplyNode is generated`() {
        val parseTree = makeParser("output 2 * 8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ MultiplyNode[ IntLiteralNode[ 2 ], IntLiteralNode[ 8 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when division then DivideNode is generated`() {
        val parseTree = makeParser("output 2 / 8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ DivideNode[ IntLiteralNode[ 2 ], IntLiteralNode[ 8 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `test add operator should by left-sided`() {
        val parseTree = makeParser("output 1 + 2 + 3;").parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "OutputCommandNode" +
                "[ " +
                    "AddNode" +
                    "[ " +
                        "AddNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ], " +
                        "IntLiteralNode[ 3 ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `test subtract operator should by left-sided`() {
        val parseTree = makeParser("output 1 - 2 - 3;").parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "OutputCommandNode" +
                "[ " +
                    "SubtractNode" +
                    "[ " +
                        "SubtractNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ], " +
                        "IntLiteralNode[ 3 ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `test multiply operator should by left-sided`() {
        val parseTree = makeParser("output 1 * 2 * 3;").parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "OutputCommandNode" +
                "[ " +
                    "MultiplyNode" +
                    "[ " +
                        "MultiplyNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ], " +
                        "IntLiteralNode[ 3 ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `test divide operator should by left-sided`() {
        val parseTree = makeParser("output 1 / 2 / 3;").parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "OutputCommandNode" +
                    "[ " +
                        "DivideNode" +
                        "[ " +
                            "DivideNode[ IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ], " +
                            "IntLiteralNode[ 3 ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `when unary minus then UnaryMinusNode is generated`() {
        val parseTree = makeParser("output -8;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ UnaryMinusNode[ IntLiteralNode[ 8 ] ] ] ]",
            parseTree.toString()
        )
    }

    //LogicalNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when logical OR then OrNode is generated`() {
        val parseTree = makeParser("output true || false;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ OrNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `test OrNode can have multiple expressions`() {
        val parseTree = makeParser("output true || false || true;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ OrNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ], BoolLiteralNode[ true ] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when logical AND then AndNode is generated`() {
        val parseTree = makeParser("output true && false;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ AndNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `test AndNode can have multiple expressions`() {
        val parseTree = makeParser("output true && false && true;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ AndNode[ [ BoolLiteralNode[ true ], BoolLiteralNode[ false ], BoolLiteralNode[ true ] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when compare equal then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 == 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] == IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `test CompareNode cannot have more than 2 expressions`() {
        val parseTree = makeParser("output 1 == 2 == 3;")
        assertFailsWith<RadkellParserException> { parseTree.parse() }
    }

    @Test
    fun `when compare not-equal then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 != 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] != IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when compare less-then then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 < 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] < IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when compare less-or-equal then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 <= 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] <= IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when compare greater-then then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 > 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] > IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when compare grater-or-equal then proper CompareNode is generated`() {
        val parseTree = makeParser("output 1 >= 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ CompareNode[ IntLiteralNode[ 1 ] >= IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when negation then NegationNode is generated`() {
        val parseTree = makeParser("output !true;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ NegationNode[ BoolLiteralNode[ true ] ] ] ]",
            parseTree.toString()
        )
    }

    //ColonOperatorNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when head operator then HeadOperatorNode is generated`() {
        val parseTree = makeParser("output head:[1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ HeadOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when tail operator then TailOperatorNode is generated`() {
        val parseTree = makeParser("output tail:[1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ TailOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when init operator then InitOperatorNode is generated`() {
        val parseTree = makeParser("output init:[1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ InitOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when last operator then LastOperatorNode is generated`() {
        val parseTree = makeParser("output last:[1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ LastOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when len operator then HeadOperatorNode is generated`() {
        val parseTree = makeParser("output len:[1, 2, 3];").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ LenOperatorNode[ IntListLiteralNode[ [1, 2, 3] ] ] ] ]",
            parseTree.toString()
        )
    }

    //CommandNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when input command then proper tree is generated`() {
        val parseTree = makeParser("input number as Int;").parse()

        assertEquals(
            "ProgramNode[ InputCommandNode[ number, INT ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when output command then proper tree is generated`() {
        val parseTree = makeParser("output 3.14;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ DoubleLiteralNode[ 3.14 ] ] ]",
            parseTree.toString()
        )
    }


    //DefinitionNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when define constant then proper tree is generated`() {
        val parseTree = makeParser("def PI: Double = 3.14;").parse()

        assertEquals(
            "ProgramNode[ ConstantDefNode[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when define simple function then proper tree is generated`() {
        val parseTree = makeParser("def foo(number: Int): Double = 1.0;").parse()

        assertEquals(
            "ProgramNode[ FunctionDefNode[ foo, DOUBLE, [ RuntimeParamDef[ number, INT ] ], DoubleLiteralNode[ 1.0 ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when define parameterless function then proper tree is generated`() {
        val parseTree = makeParser("def foo(): Int = 123;").parse()

        assertEquals(
            "ProgramNode[ FunctionDefNode[ foo, INT, [  ], IntLiteralNode[ 123 ] ] ]",
            parseTree.toString()
        )
    }

    //StatementNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when if statement then IfStatementNode is generated`() {
        val parseTree = makeParser("output if true then 1 else 2;").parse()

        assertEquals(
            "ProgramNode[ OutputCommandNode[ IfStatementNode[ BoolLiteralNode[ true ], IntLiteralNode[ 1 ], IntLiteralNode[ 2 ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when function call statement then FunctionCallNode is generated`() {
        val parseTree = makeParser(
            """
            def foo(i: Int, d: Double, b: Bool): Int = 1;
            output foo(1, 2.0, true);
        """.trimIndent()
        ).parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "FunctionDefNode[ foo, INT, [ RuntimeParamDef[ i, INT ], RuntimeParamDef[ d, DOUBLE ], RuntimeParamDef[ b, BOOL ] ], IntLiteralNode[ 1 ] ], " +
                "OutputCommandNode[ FunctionCallNode[ foo, [ IntLiteralNode[ 1 ], DoubleLiteralNode[ 2.0 ], BoolLiteralNode[ true ] ] ] ] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `test function call without parameters`() {
        val parseTree = makeParser("""
            def foo(): Int = 123;
            output foo();
        """.trimIndent()).parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "FunctionDefNode[ foo, INT, [  ], IntLiteralNode[ 123 ] ], " +
                "OutputCommandNode[ FunctionCallNode[ foo, [  ] ] ] " +
            "]",
            parseTree.toString()
        )
    }

    //ValueReadNodes.kt
    //---------------------------------------------------------
    @Test
    fun `when read constant then ConstantReadNode is generated`() {
        val parseTree = makeParser(
            """
            def PI: Double = 3.14;
            output PI;
        """.trimIndent()
        ).parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "ConstantDefNode[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ], " +
                "OutputCommandNode[ ConstantReadNode[ ConstantDef[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ] ] ] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `when read parameter then ParamReadNode is generated`() {
        val parseTree = makeParser(
            """
            def foo(number: Int): Int = number;
        """.trimIndent()
        ).parse()

        assertEquals(
            "ProgramNode[ FunctionDefNode[ foo, INT, [ RuntimeParamDef[ number, INT ] ], RuntimeParamReadNode[ RuntimeParamDef[ number, INT ] ] ] ]",
            parseTree.toString()
        )
    }

    @Test
    fun `when read input slot then InputSlotReadNode is generated`() {
        val parseTree = makeParser(
            """
            input number as Int;
            output number;
        """.trimIndent()
        ).parse()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "InputCommandNode[ number, INT ], " +
                "OutputCommandNode[ RuntimeParamReadNode[ RuntimeParamDef[ number, INT ] ] ] " +
            "]",
            parseTree.toString()
        )
    }

    //Complex test examples
    //--------------------------------------------------------
    @Test
    fun `when define constant and function then proper tree is generated`() {
        val parseTree = makeParser(
            """
            def PI: Double = 3.14;
            def circleField(r: Double): Double = PI*r*r;
        """.trimIndent()
        ).parse().typeCheck()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "ConstantDefNode[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ], " +
                "FunctionDefNode[ circleField, DOUBLE, [ RuntimeParamDef[ r, DOUBLE ] ], " +
                    "MultiplyNode" +
                    "[ " +
                        "MultiplyNode" +
                        "[ " +
                            "ConstantReadNode[ ConstantDef[ PI, DOUBLE, DoubleLiteralNode[ 3.14 ] ] ], " +
                            "RuntimeParamReadNode[ RuntimeParamDef[ r, DOUBLE ] ] " +
                        "], " +
                        "RuntimeParamReadNode[ RuntimeParamDef[ r, DOUBLE ] ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `when define recursive function then proper tree is generated`() {
        val parseTree = makeParser(
            """
            def factorial(n: Int): Int =
                if n == 0
                then 1
                else factorial(n-1) * n;
        """.trimIndent()
        ).parse().typeCheck()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "FunctionDefNode[ factorial, INT, [ RuntimeParamDef[ n, INT ] ], " +
                    "IfStatementNode" +
                    "[ " +
                        "CompareNode[ RuntimeParamReadNode[ RuntimeParamDef[ n, INT ] ] == IntLiteralNode[ 0 ] ], " +
                        "IntLiteralNode[ 1 ], " +
                        "MultiplyNode" +
                        "[ " +
                            "FunctionCallNode[ factorial, [ SubtractNode[ RuntimeParamReadNode[ RuntimeParamDef[ n, INT ] ], IntLiteralNode[ 1 ] ] ] ], " +
                            "RuntimeParamReadNode[ RuntimeParamDef[ n, INT ] ] " +
                        "] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }

    @Test
    fun `when define recursive list function then proper tree is generated`() {
        val parseTree = makeParser(
            """
            def sum(numbers: [Double]): Double =
                head:numbers + sum(tail:numbers);
        """.trimIndent()
        ).parse().typeCheck()

        assertEquals(
            "ProgramNode" +
            "[ " +
                "FunctionDefNode[ sum, DOUBLE, [ RuntimeParamDef[ numbers, DOUBLE_LIST ] ], " +
                    "AddNode" +
                    "[ " +
                        "HeadOperatorNode[ RuntimeParamReadNode[ RuntimeParamDef[ numbers, DOUBLE_LIST ] ] ], " +
                        "FunctionCallNode[ sum, [ TailOperatorNode[ RuntimeParamReadNode[ RuntimeParamDef[ numbers, DOUBLE_LIST ] ] ] ] ] " +
                    "] " +
                "] " +
            "]",
            parseTree.toString()
        )
    }
}
package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.RuntimeParamDef
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing the constant definition (not constant reading)
 */
class ConstantDefNode(
    val name: String,
    val type: TypeName,
    val initializerExpr: ExprNode,
    override val lineNumber: Int = 0
) : Node() {

    override fun accept(visitor: ParseTreeVisitor) {
        initializerExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$name, $type, $initializerExpr"
}

/**
 * Node representing the function definition (not function call)
 */
class FunctionDefNode(
    val name: String,
    val returnType: TypeName,
    val params: List<RuntimeParamDef>,
    val bodyExpr: ExprNode,
    override val lineNumber: Int = 0
) : Node() {

    override fun accept(visitor: ParseTreeVisitor) {
        bodyExpr.accept(visitor)
        visitor.visit(this)
    }

    override fun contentsToString(): String = "$name, $returnType, ${params.joinToString(prefix = "[ ", postfix = " ]")}, $bodyExpr"
}
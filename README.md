# radkell - interpreter języka #

## Autor ##
* Radosław Panuszewski

## Kluczowe cechy języka ##
* leniwy język funkcyjny
* statycznie oraz silnie typowany
* wbudowane wsparcie dla list

## Przykład kodu ##

```
def factorial(n: Int): Int =
    if n == 0
    then 1
    else factorial(n-1) * n;

def PI: Double = 3.14;

def circleField(r: Double): Double = PI*r*r;

def sum(numbers: [Int]): Int =
    if len:numbers == 0
    then 0
    else head:numbers + sum(tail:numbers);

input number as Int;
output factorial(number);
output circleField(15);
output sum([1, 2, 3, 4, 5]);
```

## Opis projektu ##
* język programowania: Kotlin
* projekt zrealizowany jako interpreter, przyjmujący na wejście plik z kodem źródłowym interpretowanego języka

## Słownik ##
* **definicja** - konstrukcja służąca do definiowania funkcji lub stałej
* **komenda** - konstrukcja realizująca pewną operację posiadającą efekty uboczne
* **operator dwukropkowy** - operator którego użycie polega na poprzedzeniu obiektu, na którym wykonywana jest operacja, nazwą operatora, zakończoną dwukropkiem (np. head:list)

## Opis języka ##
* definicje i komendy
    * program składa się wyłącznie z definicji oraz komend
    * każda definicja oraz komenda musi być zakończona średnikiem
    * w skład definicji wchodzą:
        * definicje funkcji
        * definicje stałych
    * w skład komend wchodzą:
        * komendy wejściowe (`input`)
        * komendy wyjściowe (`output`)
* wyrażenia
    * każde wyrażenie posiada wartość
    * wyrażenia konstruujemy za pomocą:
        * literałów
        * stałych
        * wywołań funkcji
        * operatorów
            * relacyjnych (`<`, `<=`, `>`, `>=`, `==`, `!=`)
            * arytmetycznych (`+`, `-`, `*`, `/`)
            * logicznych (`||`, `&&`)
            * dwukropkowych (`head`, `tail`, `init`, `last`, `len`)
* przepływ sterowania
    * instrukcje warunkowe
        * posiadają wartość
        * else jest obowiązkowy
* pętle
    * realizowane za pomocą rekurencji
* system typów
    * typowanie jest statyczne
    * `Int` - 4-bajtowe liczby całkowite
    * `Double` - 8-bajtowe liczby zmiennoprzecinkowe
    * `Bool` - wartości logiczne (prawda/fałsz)
    * `String` - łańcuchy znaków
    * `[Typ]` - oznacza listę elementów typu Typ
    * dla list zdefiniowane są dwukropkowe operatory specjalne:
        * head:lista - pierwszy element listy
        * tail:lista - wszystkie elementy za wyjątkiem pierwszego
        * init:lista - wszystkie elementy za wyjątkiem ostatniego
        * last:lista - ostatni element listy
        * len:lista - długość listy

## Konstrukcje językowe ##

definicja funkcji
```
def nazwaFunkcji(argument: TypArgumentu, ...): TypZwracany = <wyrażenie>
```

definicja stałej

```
def nazwaStałej: TypStałej = <wyrażenie>
```
komenda output (powoduje wypisanie wyrażenia na standardowe wyjście)

```
output <wyrażenie>
```
komenda input (powoduje wczytanie danych ze standardowego wejścia)
```
input nazwaStałej as Typ
```

wyrażenie warunkowe
```
if <warunek> then <wyrażenie> else <wyrażenie>
```
literał listy
```
[element1, element2, element3, ...]
```

operator dwukropkowy
```
head:[1, 2, 3]
```

## Priorytety operatorów (6 - najwyższy, 1 - najniższy) ##

| Priorytet | Grupa operatorów |
| --- | --- |
| 6 | Operatory dwukropkowe (head, tail, init, last, len) |
| 5 | Operatory multiplikatywne (*, /) |
| 4 | Operatory addytywne (+, -) |
| 3 | Operatory relacyjne (<, >, <=, >=, ==, !=) |
| 2 | Logiczne AND (&&) |
| 1 | Logiczne OR (\|\|) |


## Gramatyka w EBNF ##

```ebnf
Program = { Definition | Command };

Command = OutputCommand | InputCommand;
OutputCommand = "output" Expr ";";
InputCommand = "input" Identifier "as" Type ;

Definition = (FunctionDefinition | ConstantDefinition) ";";
ConstantDefinition = "def" Identifier ":" Type "=" Expression;
FunctionDefinition = "def" Identifier "(" ParamDefList ")" ":" Type "=" Expression;

ParamDefList = ParamDef ParamDefListPrime;
ParamDefListPrime = ", " ParamDefListPrime | E;
ParamDef = Identifier ":" Type;
Identifier = "[A-Za-z](\w)*";
Type = "Int" | "Double" | "Bool" | "[Int]" | "[Double]" | "[Bool]";

Expression = Orable { OrOperator Orable };
Orable = Andable { AndOperator Andable };
Andable = Comparable [ RelOperator Comparable ];
Comparable = Addend { AddOperator Addend };
Addend = Factor { MultOperator Factor };
Factor = [ ColonOperator ] Negable;
Negable = { NegOperator } Term;
Term = SimpleValue | "(" Expression ")";
SimpleValue = Literal | ListLiteral | Named | IfStatement;

Named = Identifier | FunctionCall;
Literal = IntLiteral | DoubleLiteral | BoolLiteral;
ListLiteral = "[" ValuesList "]";

FunctionCall = Identifier "(" ValuesList ")";
IfStatement = "if" Expression "then" Expression "else" Expression;

ValuesList =  Expression ValuesList;
ValuesListPrime = "," ValuesListPrime | E;

OrOperator = "||;
AndOperator = "&&";
RelOperator = "<" | ">" | "<=" | ">=" | "==" | "!=";
AddOperator = "+" | "-";
MultOperator = "*" | "/";
ColonOperator = "head:" | "tail:" | "init:" | "last:" | "len:";
NegOperator = "-" | "!";

IntLiteral = "-?[1-9][0-9]*";
DoubleLiteral = "-?[1-9][0-9]*\.[0-9]+";
BoolLiteral = "true" | "false";
StringLiteral = "\"(\w)*\"";
```
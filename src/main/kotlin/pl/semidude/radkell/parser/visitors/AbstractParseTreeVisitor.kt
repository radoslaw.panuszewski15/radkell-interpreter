package pl.semidude.radkell.parser.visitors

import pl.semidude.radkell.parser.*
import pl.semidude.radkell.parser.nodes.*

/**
 * Empty implementation of [ParseTreeVisitor]
 */
abstract class AbstractParseTreeVisitor : ParseTreeVisitor {

    override fun visit(parseTree: ParseTree) {}

    override fun visit(node: ProgramNode) {}

    override fun visit(node: InputCommandNode) {}

    override fun visit(node: OutputCommandNode) {}

    override fun visit(node: ConstantDefNode) {}

    override fun visit(node: FunctionDefNode) {}

    override fun visit(node: HeadOperatorNode) {}

    override fun visit(node: TailOperatorNode) {}

    override fun visit(node: InitOperatorNode) {}

    override fun visit(node: LastOperatorNode) {}

    override fun visit(node: LenOperatorNode) {}

    override fun visit(node: OrNode) {}

    override fun visit(node: AndNode) {}

    override fun visit(node: CompareNode) {}

    override fun visit(node: NegationNode) { }

    override fun visit(node: AddNode) {}

    override fun visit(node: SubtractNode) {}

    override fun visit(node: MultiplyNode) {}

    override fun visit(node: DivideNode) {}

    override fun visit(node: UnaryMinusNode) {}

    override fun visit(node: IntLiteralNode) {}

    override fun visit(node: DoubleLiteralNode) {}

    override fun visit(node: BoolLiteralNode) {}

    override fun visit(node: IntListLiteralNode) {}

    override fun visit(node: DoubleListLiteralNode) {}

    override fun visit(node: BoolListLiteralNode) {}

    override fun visit(node: IfStatementNode) {}

    override fun visit(node: ConstantReadNode) {}

    override fun visit(node: RuntimeParamReadNode) {}

    override fun visit(node: FunctionCallNode) {}
}
package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.exception.RadkellRuntimeException
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing addition
 */
class AddNode(
    val leftExpr: ExprNode,
    val rightExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(leftExpr.type) {

    override fun contentsToString(): String = "$leftExpr, $rightExpr"

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        leftExpr.accept(visitor)
        rightExpr.accept(visitor)
    }
    override fun evaluateInt(context: CallContext): Int =
        leftExpr.evaluateInt(context) + rightExpr.evaluateInt(context)

    override fun evaluateDouble(context: CallContext): Double =
        leftExpr.evaluateDouble(context) + rightExpr.evaluateDouble(context)
}

/**
 * Node representing subtraction
 */
class SubtractNode(
    val leftExpr: ExprNode,
    val rightExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(leftExpr.type) {

    override fun contentsToString(): String = "$leftExpr, $rightExpr"

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        leftExpr.accept(visitor)
        rightExpr.accept(visitor)
    }
    override fun evaluateInt(context: CallContext): Int =
        leftExpr.evaluateInt(context) - rightExpr.evaluateInt(context)

    override fun evaluateDouble(context: CallContext): Double =
        leftExpr.evaluateDouble(context) - rightExpr.evaluateDouble(context)
}

/**
 * Node representing multiplication
 */
class MultiplyNode(
    val leftExpr: ExprNode,
    val rightExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(leftExpr.type) {

    override fun contentsToString(): String = "$leftExpr, $rightExpr"

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        leftExpr.accept(visitor)
        rightExpr.accept(visitor)
    }
    override fun evaluateInt(context: CallContext): Int =
        leftExpr.evaluateInt(context) * rightExpr.evaluateInt(context)

    override fun evaluateDouble(context: CallContext): Double =
        leftExpr.evaluateDouble(context) * rightExpr.evaluateDouble(context)
}

/**
 * Node representing division
 */
class DivideNode(
    val leftExpr: ExprNode,
    val rightExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(leftExpr.type) {

    override fun contentsToString(): String = "$leftExpr, $rightExpr"

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        leftExpr.accept(visitor)
        rightExpr.accept(visitor)
    }
    override fun evaluateInt(context: CallContext): Int {
        val left = leftExpr.evaluateInt(context)
        val right = rightExpr.evaluateInt(context)
        if (right == 0) throw RadkellRuntimeException(lineNumber + 1, "division by zero!")
        return left / right
    }

    override fun evaluateDouble(context: CallContext): Double =
        leftExpr.evaluateDouble(context) / rightExpr.evaluateDouble(context)
}

/**
 * Node representing the unary minus expression
 */
class UnaryMinusNode(
    val targetExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(targetExpr.type) {

    override fun contentsToString(): String = "$targetExpr"

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        targetExpr.accept(visitor)
    }

    override fun evaluateInt(context: CallContext): Int =
        -targetExpr.evaluateInt(context)

    override fun evaluateDouble(context: CallContext): Double =
        -targetExpr.evaluateDouble(context)
}
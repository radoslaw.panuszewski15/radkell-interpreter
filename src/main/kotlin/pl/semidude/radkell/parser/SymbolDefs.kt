package pl.semidude.radkell.parser

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.nodes.ExprNode
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Each symbol definition should implement this interface.
 * Currently there are following symbols supported:
 * - function parameter - represented as [RuntimeParamDef]. Evaluated at runtime by CallContext.
 * - input slot - also represented as [RuntimeParamDef]. Because of its runtime nature, it could be treated as a parameter
 *                for global program scope (as opposed to constant which is statically evaluated)
 * - constant - represented as [ConstantDef]. Global accessible and statically evaluated.
 * - function - represented as [FunctionDef]
 */
interface SymbolDef {
    val name: String
    val type: TypeName
}

/**
 * Runtime parameter definition, i.e. parameter that can reside in CallContext. Evaluated by querying to CallContext.
 * Could be function parameter or input slot.
 */
data class RuntimeParamDef(
    override val name: String,
    override val type: TypeName,
    val scope: Scope
) : SymbolDef {
    override fun toString(): String = "RuntimeParamDef[ $name, $type ]"
}

enum class Scope { GLOBAL, LOCAL }

/**
 * Constant definition, statically evaluated.
 * Includes initializer expression which will be used to evaluate this constant's value later.
 */
data class ConstantDef(
    override val name: String,
    override val type: TypeName,
    val initializerExpr: ExprNode
) : SymbolDef {
    override fun toString(): String = "ConstantDef[ $name, $type, $initializerExpr ]"
}

/**
 * Function definition.
 * In time of declaration, body expression is in NOT DEFINED state and should be defined by call to
 * [ParseTree.bindFunctionDefinition].
 */
data class FunctionDef(
    override val name: String,
    override val type: TypeName,
    val params: List<RuntimeParamDef>,
    var bodyExpr: ExprNode = NotDefinedExpr(type)
) : SymbolDef {
    override fun toString(): String = "FunctionDef[ $name, $type, $params, $bodyExpr ]"
}

/**
 * Used to mark that the expression is not yet defined, i.e. is in NOT DEFINED state
 */
class NotDefinedExpr(type: TypeName) : ExprNode(type) {
    override val lineNumber: Int = -1
    override fun accept(visitor: ParseTreeVisitor) { }
    override fun contentsToString(): String = "NotDefinedExpr"
}
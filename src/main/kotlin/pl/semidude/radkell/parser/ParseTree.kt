package pl.semidude.radkell.parser

import pl.semidude.radkell.exception.RadkellParserException
import pl.semidude.radkell.lexer.Lexer
import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.nodes.ExprNode
import pl.semidude.radkell.parser.nodes.Node
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * A parse tree which is the result of parsing the source code.
 * Contains node tree structure as well as symbol definitions (constant, functions and input slots).
 */
class ParseTree(private val lexer: Lexer) {

    companion object {
        /**
         * The global scope of the program
         */
        val GLOBAL_SCOPE = FunctionDef("GLOBAL_SCOPE", TypeName.INT, emptyList())
    }

    /**
     * Global context of the program, could contain only input slots (constants are maintained in a static way)
     */
    var globalContext = CallContext(mutableMapOf())

    /**
     * The root of the nodes tree structure
     */
    lateinit var rootNode: Node

    private val symbols = mutableMapOf<String, SymbolDef>()
    private var curScope: FunctionDef = GLOBAL_SCOPE

    fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        rootNode.accept(visitor)
    }

    /**
     * Define a constant
     * @param name name of the constant
     * @param type type of the constant
     * @param initializerExpr expression which will be used to evaluate value of the constant (in a lazy way)
     */
    fun defineConstant(name: String, type: TypeName, initializerExpr: ExprNode) {
        symbols[name] = ConstantDef(name, type, initializerExpr)
    }

    /**
     * Define a input slot
     * @param name name of the input slot
     * @param type type of the inout slot
     */
    fun defineInputSlot(name: String, type: TypeName) {
        symbols[name] = RuntimeParamDef(name, type, Scope.GLOBAL)
    }

    /**
     * Declare function, by providing only the signature. The body of the function will be provided later
     * By calling this method, the [ParseTree] object will enter scope of the function being declared
     * @param name name of the function
     * @param returnType return type of the function
     * @param params parameter definitions of the function
     */
    fun declareFunction(name: String, returnType: TypeName, params: List<RuntimeParamDef>) {
        val function = FunctionDef(name, returnType, params)
        symbols[name] = function
        curScope = function
        params.forEach {
            symbols[it.name] = RuntimeParamDef(it.name, it.type, Scope.LOCAL)
        }
    }

    /**
     * Provide body for the function declared earlier
     * By calling this method, the [ParseTree] object will leave the scope of function and go back to global scope
     * @param name name of the function
     * @param bodyExpr body of the function
     */
    fun bindFunctionDefinition(name: String, bodyExpr: ExprNode) {
        (symbols[name] as FunctionDef).bodyExpr = bodyExpr
        curScope = GLOBAL_SCOPE
    }

    /**
     * Get identifier symbol definition by its name. Identifier symbol could be constant or parameter.
     * @param name name of the symbol
     * @return symbol definition
     */
    fun getIdentifierSymbol(name: String): SymbolDef {
        ensureSymbolDefined(name)
        val symbol = symbols.getValue(name)
        ensureSymbolInScope(symbol, name)
        return symbol
    }

    /**
     * Get function definition by its name.
     * @param name name of the function
     * @return function definition
     */
    fun getFunctionDef(name: String): FunctionDef {
        ensureSymbolDefined(name)
        val symbol = symbols.getValue(name)
        return ensureSymbolIsFunction(symbol)
    }

    private fun ensureSymbolDefined(name: String) {
        if (!symbols.containsKey(name))
            throwParserException("symbol $name is not defnied")
    }

    private fun ensureSymbolInScope(symbol: SymbolDef, name: String) {
        if (symbol is RuntimeParamDef && symbol.scope == Scope.LOCAL && !isInScope(symbol))
            throwParserException("symbol $name is not accessible in scope ${curScope.name}")
    }

    private fun isInScope(param: RuntimeParamDef): Boolean =
        curScope == GLOBAL_SCOPE || curScope.params.contains(param)

    private fun ensureSymbolIsFunction(symbol: SymbolDef) =
         symbol as? FunctionDef ?: throwParserException("symbol ${symbol.name} is not a functionDef")

    private fun throwParserException(message: String): Nothing {
        throw RadkellParserException(lexer.getLastTokenLineNumber(), message)
    }

    override fun toString(): String {
        return rootNode.toString()
    }
}
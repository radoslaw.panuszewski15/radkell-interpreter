package pl.semidude.radkell.exception

import java.lang.Exception

class RadkellParserException(val lineNumber: Int, message: String)
    : Exception("[COMPILE ERROR] [line: $lineNumber] $message")

class RadkellRuntimeException(val lineNumber:Int, message: String?)
    : Exception("[RUNTIME ERROR]${if (lineNumber > 0) " [line: $lineNumber]" else ""} $message")

class RadkellLexerException(val lineNumber: Int, message: String)
    : Exception("[COMPILE ERROR] [line: $lineNumber] $message")
package pl.semidude.radkell.parser

import org.junit.Test
import pl.semidude.radkell.exception.RadkellParserException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import pl.semidude.radkell.helpers.*

class TypeCheckTest {

    //ProgramNode
    @Test
    fun `when top levels nodes are not only commands and definitions then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("if true then 1 else 2").parse().typeCheck()
        }
    }

    //ConstantDefNode
    @Test
    fun `when define constant and assign wrong type then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("def PI: Int = 3.14;").parse().typeCheck()
        }
    }

    //FunctionDefNode
    @Test
    fun `when body expression does match function return type then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("def foo(): Int = 3.14;").parse().typeCheck()
        }
    }

    //HeadOperatorNode
    @Test
    fun `when apply non-list value to head operator then then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output head:1;").parse().typeCheck()
        }
    }

    //TailOperatorNode
    @Test
    fun `when apply non-list value to tail operator then then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output tail:1;").parse().typeCheck()
        }
    }

    //InitOperatorNode
    @Test
    fun `when apply non-list value to init operator then then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output init:1;").parse().typeCheck()
        }
    }

    //LastOperatorNode
    @Test
    fun `when apply non-list value to last operator then then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output last:1;").parse().typeCheck()
        }
    }

    //LenOperatorNode
    @Test
    fun `when apply non-list value to len operator then then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output len:1;").parse().typeCheck()
        }
    }

    //OrNode
    @Test
    fun `when argument of logical OR is not boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output true || 1;").parse().typeCheck()
        }
    }

    //AndNode
    @Test
    fun `when argument of logical AND is not boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output true && 1;").parse().typeCheck()
        }
    }

    //CompareNode
    @Test
    fun `when compare values of types int and double then ok`() {
        makeParser("output 1 < 2.0;").parse().typeCheck()
    }

    @Test
    fun `when compare values of types int and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1 < true;").parse().typeCheck()
        }
    }

    @Test
    fun `when compare values of types double and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1.0 < true;").parse().typeCheck()
        }
    }

    @Test
    fun `when compare values of types boolean and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output false < true;").parse().typeCheck()
        }
    }

    @Test
    fun `when use negation on int value then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output !20;").parse().typeCheck()
        }
    }

    @Test
    fun `when use negation on double value then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output !20.0;").parse().typeCheck()
        }
    }

    //AddNode
    @Test
    fun `when add values of types int and double then ok`() {
        makeParser("output 1 + 2.0;").parse().typeCheck()
    }

    @Test
    fun `when add values of types int and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1 + true;").parse().typeCheck()
        }
    }

    @Test
    fun `when add values of types double and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1.0 + true;").parse().typeCheck()
        }
    }

    @Test
    fun `when add values of types boolean and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output false + true;").parse().typeCheck()
        }
    }

    //SubtractNode
    @Test
    fun `when subtract values of types int and double then ok`() {
        makeParser("output 1 - 2.0;").parse().typeCheck()
    }

    @Test
    fun `when subtract values of types int and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1 - true;").parse().typeCheck()
        }
    }

    @Test
    fun `when subtract values of types double and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1.0 - true;").parse().typeCheck()
        }
    }

    @Test
    fun `when subtract values of types boolean and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output false - true;").parse().typeCheck()
        }
    }

    //MultiplyNode
    @Test
    fun `when multiply values of types int and double then ok`() {
        makeParser("output 1 * 2.0;").parse().typeCheck()
    }

    @Test
    fun `when multiply values of types int and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1 * true;").parse().typeCheck()
        }
    }

    @Test
    fun `when multiply values of types double and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1.0 * true;").parse().typeCheck()
        }
    }

    @Test
    fun `when multiply values of types boolean and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output false * true;").parse().typeCheck()
        }
    }

    //DivideNode
    @Test
    fun `when divide values of types int and double then ok`() {
        makeParser("output 1 / 2.0;").parse().typeCheck()
    }

    @Test
    fun `when divide values of types int and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1 / true;").parse().typeCheck()
        }
    }

    @Test
    fun `when divide values of types double and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output 1.0 / true;").parse().typeCheck()
        }
    }

    @Test
    fun `when divide values of types boolean and boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output false / true;").parse().typeCheck()
        }
    }

    //UnaryMinusNode
    @Test
    fun `when apply minus to boolean value then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output -true;").parse().typeCheck()
        }
    }

    //IfStatement
    @Test
    fun `when if condition isn't of type boolean then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output if 1 then 1 else 2").parse().typeCheck()
        }
    }

    @Test
    fun `when if branches has different types then error`() {
        assertFailsWith<RadkellParserException> {
            makeParser("output if true then 1 lese 2.0").parse().typeCheck()
        }
    }

    //FunctionCallNode
    @Test
    fun `when call function with wrong parameters then error`() {
        val parser = makeParser(
            """
            def foo(i: Int, d: Double, b: Bool): Int = 1;
            output foo(3.14, false, 1);
        """.trimIndent()
        )

        assertFailsWith<RadkellParserException> { parser.parse().typeCheck() }
    }

    //error messages
    @Test
    fun `test if error line number is valid 1`() {
        try {
            makeParser("""
                output number;
                output if true then 1 else 2;
                def A: Int = 8.0; """.trimIndent()
            ).parse().typeCheck()
        }
        catch (e: RadkellParserException) {
            assertEquals(1, e.lineNumber)
        }
    }

    @Test
    fun `test if error line number is valid 2`() {
        try {
            makeParser("""
                output 1;
                if true then 1 else 2;
                def A: Int = 8; """.trimIndent()
            ).parse().typeCheck()
        }
        catch (e: RadkellParserException) {
            assertEquals(2, e.lineNumber)
        }
    }

    @Test
    fun `test if error line number is valid 3`() {
        try {
            makeParser("""
                output 1;
                output if true then 1 else 2;
                def A: Int = 8.0; """.trimIndent()
            ).parse().typeCheck()
        }
        catch (e: RadkellParserException) {
            assertEquals(3, e.lineNumber)
        }
    }
}
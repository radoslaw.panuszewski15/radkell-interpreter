package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.parser.FunctionDef
import pl.semidude.radkell.parser.evaluation.Evaluable
import pl.semidude.radkell.parser.evaluation.LazyEvaluator
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing a function call (not function definition)
 */
class FunctionCallNode(
    val functionDef: FunctionDef,
    val paramValues: List<ExprNode>,
    override val lineNumber: Int = 0
) : ExprNode(functionDef.type),

    Evaluable by LazyEvaluator(
        evaluableProvider = { functionDef.bodyExpr },
        contextPreprocessor = { context ->
            context.includeParams(functionDef.params, paramValues)
        }
    )
{
    override fun accept(visitor: ParseTreeVisitor) = visitor.visit(this)

    override fun contentsToString(): String =
        "${functionDef.name}, ${paramValues.joinToString(", ", "[ ", " ]")}"
}

/**
 * Node representing an if statement expression
 */
class IfStatementNode(
    val condExpr: ExprNode,
    val thenExpr: ExprNode,
    val elseExpr: ExprNode,
    override val lineNumber: Int = 0
) : ExprNode(thenExpr.type),

    Evaluable by LazyEvaluator(
        evaluableProvider = { context ->
            if (condExpr.evaluateBool(context)) thenExpr
            else elseExpr
        }
    )
{
    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        condExpr.accept(visitor)
        thenExpr.accept(visitor)
        elseExpr.accept(visitor)
    }

    override fun contentsToString(): String =
        "$condExpr, $thenExpr, $elseExpr"
}
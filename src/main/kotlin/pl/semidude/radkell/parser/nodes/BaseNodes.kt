package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.lexer.TypeName.*
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.evaluation.Evaluable
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Base class for all nodes
 */
abstract class Node {
    abstract fun accept(visitor: ParseTreeVisitor)
    abstract val lineNumber: Int
    override fun toString(): String = "${this::class.simpleName}[ ${contentsToString()} ]"
    abstract fun contentsToString(): String
}

/**
 * Top-level node representing the program
 */
class ProgramNode(
    val nodes: List<Node>,
    override val lineNumber: Int = 0
) : Node() {

    override fun accept(visitor: ParseTreeVisitor) {
        visitor.visit(this)
        nodes.forEach { it.accept(visitor) }
    }

    override fun contentsToString(): String =
        nodes.joinToString(", ") { it.toString() }
}

/**
 * Node representing some kind of expression, could be evaluated. Default [Evaluable] implementation throws exceptions,
 * thus subclasses should provide their own implementation of that interface (possibly using evaluator mixins)
 */
abstract class ExprNode(val type: TypeName) : Node(), Evaluable {

    fun evaluate(context: CallContext): Any =
        when (type) {
            INT -> evaluateInt(context)
            DOUBLE -> evaluateDouble(context)
            BOOL -> evaluateBool(context)
            INT_LIST -> evaluateIntList(context)
            DOUBLE_LIST -> evaluateDoubleList(context)
            BOOL_LIST -> evaluateBoolList(context)
        }

    fun compareTo(other: ExprNode, context: CallContext): Int =
        when  {
            type == INT && other.type == INT -> compareValues(this.evaluateInt(context), other.evaluateInt(context))
            type == DOUBLE && other.type == DOUBLE -> compareValues(this.evaluateDouble(context), other.evaluateDouble(context))
            type == BOOL && other.type == BOOL -> compareValues(this.evaluateBool(context), other.evaluateBool(context))
            type == INT && other.type == DOUBLE -> compareValues(this.evaluateInt(context).toDouble(), other.evaluateDouble(context))
            type == DOUBLE && other.type == INT -> compareValues(this.evaluateDouble(context), other.evaluateInt(context).toDouble())
            else -> error("$type adn ${other.type} types comparision not supported")
        }
}
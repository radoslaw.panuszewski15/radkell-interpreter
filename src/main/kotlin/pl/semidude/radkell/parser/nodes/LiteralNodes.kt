package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.parser.evaluation.CallContext
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node representing the Int literal
 */
class IntLiteralNode(
    val value: Int,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.INT) {

    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateInt(context: CallContext): Int = value
}

/**
 * Node representing the Double literal
 */
class DoubleLiteralNode(
    val value: Double,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.DOUBLE) {

    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateDouble(context: CallContext): Double = value
}

/**
 * Node representing the Bool literal
 */
class BoolLiteralNode(
    val value: Boolean,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.BOOL) {

    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateBool(context: CallContext): Boolean = value
}

/**
 * Node representing the Int list literal
 */
class IntListLiteralNode(
    val value: List<Int>,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.INT_LIST) {
    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateIntList(context: CallContext): List<Int> = value
}

/**
 * Node representing the Double list literal
 */
class DoubleListLiteralNode(
    val value: List<Double>,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.DOUBLE_LIST) {

    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateDoubleList(context: CallContext): List<Double> = value
}

/**
 * Node representing the Bool list literal
 */
class BoolListLiteralNode(
    val value: List<Boolean>,
    override val lineNumber: Int = 0
) : ExprNode(TypeName.BOOL_LIST) {

    override fun accept(visitor: ParseTreeVisitor) { visitor.visit(this) }
    override fun contentsToString(): String = "$value"
    override fun evaluateBoolList(context: CallContext): List<Boolean> = value
}
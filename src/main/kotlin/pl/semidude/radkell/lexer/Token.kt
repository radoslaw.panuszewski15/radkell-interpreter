package pl.semidude.radkell.lexer

import pl.semidude.radkell.lexer.TokenDescriptor.*
import java.lang.IllegalArgumentException

/**
 * Token abstraction
 */
open class Token(
    val descriptor: TokenDescriptor
) {
    val correspondingTypeName: TypeName get() = descriptor.mapToCorrespondingTypeName()
    override fun toString() = "[${descriptor.name}]"
    open fun errorMessageName() = descriptor.description
}

/**
 * Token which has a value of type T
 */
open class ValueToken<T: Any>(
    descriptor: TokenDescriptor,
    val content: T
) : Token(descriptor) {
    override fun toString() = "[${descriptor.name}] $content"
    override fun errorMessageName(): String = toString()
}

class IntLiteral(value: Int) : ValueToken<Int>(INT_LITERAL, value)
class DoubleLiteral(value: Double) : ValueToken<Double>(DOUBLE_LITERAL, value)
class BoolLiteral(value: Boolean) : ValueToken<Boolean>(BOOL_LITERAL, value)
class Identifier(content: String) : ValueToken<String>(IDENTIFIER, content)

/**
 * Enum which describes type of the token
 */
enum class TokenDescriptor(val stringValue: String, val description: String = "'$stringValue'") {
    IDENTIFIER("", "Identifier"),
    INT_LITERAL("", "IntLiteral"),
    DOUBLE_LITERAL("", "DoubleLiteral"),
    BOOL_LITERAL("", "BoolLiteral"),
    DEF_KW("def"),
    IF_KW("if"),
    THEN_KW("then"),
    ELSE_KW("else"),
    INPUT_KW("input"),
    OUTPUT_KW("output"),
    AS_KW("as"),
    COLON(":"),
    SEMICOLON(";"),
    COMMA(","),
    DOT("."),
    OPEN_PARENTHESIS("("),
    CLOSE_PARENTHESIS(")"),
    OPEN_SQUARE_BRACKET("["),
    CLOSE_SQUARE_BRACKET("]"),
    INT_TYPE("Int"),
    DOUBLE_TYPE("Double"),
    BOOL_TYPE("Bool"),
    INT_LIST_TYPE("[Int]"),
    DOUBLE_LIST_TYPE("[Double]"),
    BOOL_LIST_TYPE("[Bool]"),
    ASSIGN_OP("="),
    LESS_THAN_OP("<"),
    GREATER_THAN_OP(">"),
    LESS_EQUAL_OP("<="),
    GREATER_EQUALS_OP(">="),
    EQUALS_OP("=="),
    NOT_EQUALS_OP("!="),
    ADD_OP("+"),
    SUBTRACT_OP("-"),
    MULTIPLY_OP("*"),
    DIVIDE_OP("/"),
    OR_OP("||"),
    AND_OP("&&"),
    NOT_OP("!"),
    HEAD_OP("head:"),
    TAIL_OP("tail:"),
    INIT_OP("init:"),
    LAST_OP("last:"),
    LEN_OP("len:"),
    END("$", "EOT (end of text)")
}

/**
 * Enumerates all the types supported in interpreted language
 */
enum class TypeName {
    INT, DOUBLE, BOOL, INT_LIST, DOUBLE_LIST, BOOL_LIST
}

/**
 * Map literal tokens and type tokens to corresponding type name from [TypeName] enum
 */
fun TokenDescriptor.mapToCorrespondingTypeName(): TypeName = when(this) {
    INT_LITERAL, INT_TYPE -> TypeName.INT
    DOUBLE_LITERAL, DOUBLE_TYPE -> TypeName.DOUBLE
    BOOL_LITERAL, BOOL_TYPE -> TypeName.BOOL
    INT_LIST_TYPE -> TypeName.INT_LIST
    DOUBLE_LIST_TYPE -> TypeName.DOUBLE_LIST
    BOOL_LIST_TYPE -> TypeName.BOOL_LIST
    else -> throw IllegalArgumentException("descriptor $this does not correspond to any TypeName")
}

/**
 * Map type name to its literal type (makes collection type a non-collection type,
 * non-collection types are returned as they are)
 */
fun TypeName.mapToElementType(): TypeName = when(this) {
    TypeName.INT -> TypeName.INT
    TypeName.DOUBLE -> TypeName.DOUBLE
    TypeName.BOOL -> TypeName.BOOL
    TypeName.INT_LIST -> TypeName.INT
    TypeName.DOUBLE_LIST -> TypeName.DOUBLE
    TypeName.BOOL_LIST -> TypeName.BOOL
}

/**
 * Map type name to its literal descriptor
 */
fun TypeName.mapToCorrespondingLiteralDescriptor(): TokenDescriptor = when(this) {
    TypeName.INT -> INT_LITERAL
    TypeName.DOUBLE -> DOUBLE_LITERAL
    TypeName.BOOL -> BOOL_LITERAL
    TypeName.INT_LIST -> INT_LITERAL
    TypeName.DOUBLE_LIST -> DOUBLE_LITERAL
    TypeName.BOOL_LIST -> BOOL_LITERAL
}
package pl.semidude.radkell.parser.visitors

import pl.semidude.radkell.parser.*
import pl.semidude.radkell.parser.nodes.*

/**
 * Visitor interface for visiting node tree
 */
interface ParseTreeVisitor {
    fun visit(parseTree: ParseTree)
    fun visit(node: ProgramNode)
    fun visit(node: InputCommandNode)
    fun visit(node: OutputCommandNode)
    fun visit(node: ConstantDefNode)
    fun visit(node: FunctionDefNode)
    fun visit(node: HeadOperatorNode)
    fun visit(node: TailOperatorNode)
    fun visit(node: InitOperatorNode)
    fun visit(node: LastOperatorNode)
    fun visit(node: LenOperatorNode)
    fun visit(node: OrNode)
    fun visit(node: AndNode)
    fun visit(node: CompareNode)
    fun visit(node: NegationNode)
    fun visit(node: AddNode)
    fun visit(node: SubtractNode)
    fun visit(node: MultiplyNode)
    fun visit(node: DivideNode)
    fun visit(node: UnaryMinusNode)
    fun visit(node: IntLiteralNode)
    fun visit(node: DoubleLiteralNode)
    fun visit(node: BoolLiteralNode)
    fun visit(node: IntListLiteralNode)
    fun visit(node: DoubleListLiteralNode)
    fun visit(node: BoolListLiteralNode)
    fun visit(node: IfStatementNode)
    fun visit(node: ConstantReadNode)
    fun visit(node: RuntimeParamReadNode)
    fun visit(node: FunctionCallNode)
}
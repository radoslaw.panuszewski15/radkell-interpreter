package pl.semidude.radkell.parser

import pl.semidude.radkell.exception.RadkellParserException
import pl.semidude.radkell.helpers.buildList
import pl.semidude.radkell.lexer.*
import pl.semidude.radkell.lexer.TokenDescriptor.*
import pl.semidude.radkell.parser.nodes.*
import java.lang.IllegalStateException

/**
 * Class which performs syntactic analysis of the set of tokens obtained from [Lexer]
 */
class Parser(private val lexer: Lexer) {

    private lateinit var curToken: Token
    private lateinit var parseTree: ParseTree
    val lineNumber: Int get() = lexer.getLastTokenLineNumber() - 1

    fun parse(): ParseTree {
        nextToken()
        parseTree = ParseTree(lexer)
        parseTree.rootNode = program()
        return parseTree
    }

    private fun program(): Node {
        val childNodes = buildList<Node> {
            while (lexer.hasNext()) {
                add(when {
                    checkToken(INPUT_KW, OUTPUT_KW) -> command()
                    checkToken(DEF_KW) -> definition()
                    else -> errorExpected("command or definition")
                })
            }
        }
        return ProgramNode(childNodes, lineNumber)
    }

    private fun command(): Node {
        return when {
            checkToken(INPUT_KW) -> inputCommand()
            checkToken(OUTPUT_KW) -> outputCommand()
            else -> errorExpected(INPUT_KW, OUTPUT_KW)
        }
    }

    private fun inputCommand(): InputCommandNode {

        fun parseName(): String {
            expectToken(INPUT_KW)
            val identifier = expectToken(IDENTIFIER) as Identifier
            expectToken(AS_KW)
            return identifier.content
        }

        fun parseType(): TypeName {
            val type = expectToken(
                INT_TYPE, DOUBLE_TYPE, BOOL_TYPE,
                INT_LIST_TYPE, DOUBLE_LIST_TYPE, BOOL_LIST_TYPE
            ).correspondingTypeName
            expectToken(SEMICOLON)
            return type
        }

        val commandName = parseName()
        val type = parseType()
        parseTree.defineInputSlot(commandName, type)

        return InputCommandNode(commandName, type, lineNumber)
    }

    private fun outputCommand(): OutputCommandNode {

        fun parseExpression(): ExprNode {
            expectToken(OUTPUT_KW)
            val expr = expression()
            expectToken(SEMICOLON)
            return expr
        }

        return OutputCommandNode(parseExpression(), lineNumber)
    }

    private fun definition(): Node {
        expectToken(DEF_KW)
        expectToken(IDENTIFIER)
        return when {
            checkToken(COLON) -> {
                unconsumeToken()
                unconsumeToken()
                constantDefinition()
            }
            checkToken(OPEN_PARENTHESIS) -> {
                unconsumeToken()
                unconsumeToken()
                functionDefinition()
            }
            else -> errorExpected(COLON, OPEN_PARENTHESIS)
        }
    }

    private fun constantDefinition(): ConstantDefNode {

        fun parseName(): String {
            expectToken(DEF_KW)
            val identifier = expectToken(IDENTIFIER) as Identifier
            expectToken(COLON)
            return identifier.content
        }

        fun parseType(): TypeName {
            return expectToken(
                INT_TYPE, DOUBLE_TYPE, BOOL_TYPE,
                INT_LIST_TYPE, DOUBLE_LIST_TYPE, BOOL_LIST_TYPE
            ).correspondingTypeName
        }

        fun parseInitializerExpression(): ExprNode {
            expectToken(ASSIGN_OP)
            val initializerExpr = expression()
            expectToken(SEMICOLON)
            return initializerExpr
        }

        val constantName = parseName()
        val type = parseType()
        val initializerExpr = parseInitializerExpression()
        parseTree.defineConstant(constantName, type, initializerExpr)

        return ConstantDefNode(constantName, type, initializerExpr, lineNumber)
    }

    private fun functionDefinition(): FunctionDefNode {

        fun parseName(): String {
            expectToken(DEF_KW)
            return (expectToken(IDENTIFIER) as Identifier).content
        }

        fun parseParameters(): List<RuntimeParamDef> {
            expectToken(OPEN_PARENTHESIS)
            val params = paramDefList()
            expectToken(CLOSE_PARENTHESIS)
            expectToken(COLON)
            return params
        }

        fun parseReturnType(): TypeName {
            return expectToken(
                INT_TYPE, DOUBLE_TYPE, BOOL_TYPE,
                INT_LIST_TYPE, DOUBLE_LIST_TYPE, BOOL_LIST_TYPE
            ).correspondingTypeName
        }

        fun parseBodyExpression(): ExprNode {
            expectToken(ASSIGN_OP)
            val bodyExpr = expression()
            expectToken(SEMICOLON)
            return bodyExpr
        }

        val functionName = parseName()
        val params = parseParameters()
        val returnType = parseReturnType()
        parseTree.declareFunction(functionName, returnType, params)

        val bodyExpr = parseBodyExpression()
        parseTree.bindFunctionDefinition(functionName, bodyExpr)

        return FunctionDefNode(functionName, returnType, params, bodyExpr, lineNumber)
    }

    private fun paramDefList(accumulatorList: MutableList<RuntimeParamDef> = mutableListOf()): List<RuntimeParamDef> {
        if (!checkToken(IDENTIFIER))
            return emptyList()

        accumulatorList += paramDef()

        if (checkToken(COMMA)) {
            consumeToken()
            paramDefList(accumulatorList)
        }
        return accumulatorList
    }

    private fun paramDef(): RuntimeParamDef {

        fun parseName(): String {
            val identifier = expectToken(IDENTIFIER) as Identifier
            expectToken(COLON)
            return identifier.content
        }

        fun parseType(): TypeName {
            return expectToken(
                INT_TYPE, DOUBLE_TYPE, BOOL_TYPE,
                INT_LIST_TYPE, DOUBLE_LIST_TYPE, BOOL_LIST_TYPE
            ).correspondingTypeName
        }

        return RuntimeParamDef(parseName(), parseType(), Scope.LOCAL)
    }

    private fun expression(): ExprNode {
        val childExprs = mutableListOf<ExprNode>()
        childExprs += orable()

        while (checkToken(OR_OP)) {
            consumeToken()
            childExprs += orable()
        }
        return if (childExprs.size > 1) OrNode(childExprs, lineNumber)
               else childExprs.first()
    }

    private fun orable(): ExprNode {
        val childExprs = mutableListOf<ExprNode>()
        childExprs += andable()

        while (checkToken(AND_OP)) {
            consumeToken()
            childExprs += andable()
        }
        return if (childExprs.size > 1) AndNode(childExprs, lineNumber)
               else childExprs.first()
    }

    private fun andable(): ExprNode {
        val leftExpr =  comparable()
        return if (checkToken(LESS_THAN_OP, GREATER_THAN_OP, LESS_EQUAL_OP, GREATER_EQUALS_OP, EQUALS_OP, NOT_EQUALS_OP)) {
            val operatorToken = consumeToken()
            val rightExpr = comparable()

            CompareNode(leftExpr, rightExpr, operatorToken.descriptor, lineNumber)
        }
        else leftExpr
    }

    private fun comparable(): ExprNode {
        var leftExpr = addend()

        while (checkToken(ADD_OP, SUBTRACT_OP)) {
            val operatorToken = consumeToken()
            val rightExpr = addend()

            leftExpr = when (operatorToken.descriptor) {
                ADD_OP -> AddNode(leftExpr, rightExpr, lineNumber)
                SUBTRACT_OP -> SubtractNode(leftExpr, rightExpr, lineNumber)
                else -> error("internal parser error")
            }
        }
        return leftExpr
    }

    private fun addend(): ExprNode {
        var leftExpr = factor()

        while (checkToken(MULTIPLY_OP, DIVIDE_OP)) {
            val operatorToken = consumeToken()
            val rightExpr = factor()

            leftExpr = when (operatorToken.descriptor) {
                MULTIPLY_OP -> MultiplyNode(leftExpr, rightExpr, lineNumber)
                DIVIDE_OP -> DivideNode(leftExpr, rightExpr, lineNumber)
                else -> error("internal parser error")
            }
        }
        return leftExpr
    }

    private fun factor(): ExprNode {
        return if (checkToken(HEAD_OP, TAIL_OP, INIT_OP, LAST_OP, LEN_OP)) {
            val operatorToken = consumeToken()
            val targetExpr = negable()

            when (operatorToken.descriptor) {
                HEAD_OP -> HeadOperatorNode(targetExpr, lineNumber)
                TAIL_OP -> TailOperatorNode(targetExpr, lineNumber)
                INIT_OP -> InitOperatorNode(targetExpr, lineNumber)
                LAST_OP -> LastOperatorNode(targetExpr, lineNumber)
                LEN_OP -> LenOperatorNode(targetExpr, lineNumber)
                else -> error("internal parser error")
            }
        } else negable()
    }

    private fun negable(): ExprNode {
        return when {
            checkToken(SUBTRACT_OP) -> processSelfNestableNode(SUBTRACT_OP, ::UnaryMinusNode)
            checkToken(NOT_OP) -> processSelfNestableNode(NOT_OP, ::NegationNode)
            else -> term()
        }
    }

    private fun <T : ExprNode> processSelfNestableNode(tokenDescriptor: TokenDescriptor, nodeCreator: (ExprNode, Int) -> T): T {
        expectToken(tokenDescriptor)
        var extraOperatorCounter = 0

        while (checkToken(tokenDescriptor)) {
            consumeToken()
            extraOperatorCounter++
        }

        var node = nodeCreator(term(), lineNumber)
        for (i in 0 until extraOperatorCounter)
            node = nodeCreator(node, lineNumber)

        return node
    }

    private fun term(): ExprNode {
        return if (checkToken(OPEN_PARENTHESIS)) {
            consumeToken()
            val expr = expression()
            expectToken(CLOSE_PARENTHESIS)
            expr
        }
        else simpleValue()
    }

    private fun simpleValue(): ExprNode {
        return when {
            checkToken(INT_LITERAL, DOUBLE_LITERAL, BOOL_LITERAL) -> literal()
            checkToken(OPEN_SQUARE_BRACKET) -> listLiteral()
            checkToken(IDENTIFIER) -> named()
            checkToken(IF_KW) -> ifStatement()
            else -> errorExpected(
                INT_LITERAL, DOUBLE_LITERAL, BOOL_LITERAL, IDENTIFIER, IF_KW
            )
        }
    }

    private fun literal(): ExprNode {
        return consumeToken().let { token ->
            when (token.correspondingTypeName) {
                TypeName.INT -> IntLiteralNode((token as IntLiteral).content, lineNumber)
                TypeName.DOUBLE -> DoubleLiteralNode((token as DoubleLiteral).content, lineNumber)
                TypeName.BOOL -> BoolLiteralNode((token as BoolLiteral).content, lineNumber)
                else -> error("internal parser error")
            }
        }
    }

    private fun listLiteral(): ExprNode {
        expectToken(OPEN_SQUARE_BRACKET)

        val expr: ExprNode = when {
            checkToken(INT_LITERAL) -> typedListLiteral<Int>(TypeName.INT)
            checkToken(DOUBLE_LITERAL) -> typedListLiteral<Double>(TypeName.DOUBLE)
            checkToken(BOOL_LITERAL) -> typedListLiteral<Boolean>(TypeName.BOOL)
            else -> errorExpected(INT_LITERAL, DOUBLE_LITERAL, BOOL_LITERAL)
        }
        expectToken(CLOSE_SQUARE_BRACKET)
        return expr
    }

    /**
     * Type has been checked already in [listLiteral] method
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T> typedListLiteral(type: TypeName, accumulatorList: MutableList<T> = mutableListOf()): ExprNode {
        expectToken(type.mapToCorrespondingLiteralDescriptor()).also { token ->
            when (token) {
                is IntLiteral -> accumulatorList += token.content as T
                is DoubleLiteral -> accumulatorList += token.content as T
                is BoolLiteral -> accumulatorList += token.content as T
            }
        }
        if (checkToken(COMMA)) {
            consumeToken()
            typedListLiteral(type, accumulatorList)
        }
        return when(type) {
            TypeName.INT -> IntListLiteralNode(accumulatorList as List<Int>, lineNumber)
            TypeName.DOUBLE -> DoubleListLiteralNode(accumulatorList as List<Double>, lineNumber)
            TypeName.BOOL -> BoolListLiteralNode(accumulatorList as List<Boolean>, lineNumber)
            else -> error("internal parser error")
        }
    }

    private fun named(): ExprNode {
        expectToken(IDENTIFIER)

        return if (checkToken(OPEN_PARENTHESIS)) {
            unconsumeToken()
            functionCall()
        } else {
            unconsumeToken()
            identifier()
        }
    }

    private fun identifier(): ExprNode {
        val identifier = consumeToken() as Identifier

        return parseTree.getIdentifierSymbol(identifier.content).let { symbolDef ->
            when (symbolDef) {
                is ConstantDef -> ConstantReadNode(symbolDef, lineNumber)
                is RuntimeParamDef -> RuntimeParamReadNode(symbolDef, lineNumber)
                else -> error("internal parser error")
            }
        }
    }

    private fun functionCall(): FunctionCallNode {

        fun parseName(): String {
            return (expectToken(IDENTIFIER) as Identifier).content
        }
        fun parseParameters(): List<ExprNode> {
            expectToken(OPEN_PARENTHESIS)
            val paramValues =
                if (!checkToken(CLOSE_PARENTHESIS)) valuesList()
                else emptyList()
            expectToken(CLOSE_PARENTHESIS)
            return paramValues
        }
        val functionName = parseName()
        val paramValues = parseParameters()

        return FunctionCallNode(parseTree.getFunctionDef(functionName), paramValues, lineNumber)
    }

    private fun valuesList(accumulatorList: MutableList<ExprNode> = mutableListOf()): List<ExprNode> {
        accumulatorList += expression()

        if (checkToken(COMMA)) {
            consumeToken()
            valuesList(accumulatorList)
        }
        return accumulatorList
    }

    private fun ifStatement(): ExprNode {
        expectToken(IF_KW)
        val condExpr = expression()
        expectToken(THEN_KW)
        val thenExpr = expression()
        expectToken(ELSE_KW)
        val elseExpr = expression()

        return IfStatementNode(condExpr, thenExpr, elseExpr, lineNumber)
    }

    private fun nextToken() {
        curToken = if (lexer.hasNext()) lexer.getNextToken()!!
        else throw RadkellParserException(
            lexer.getLastTokenLineNumber(),
            "no more tokens"
        )
    }

    private fun unconsumeToken() {
        curToken = lexer.ungetToken() ?: throw IllegalStateException("unconsuming first curToken is not allowed")
    }

    private fun consumeToken(): Token {
        val consumed = curToken
        nextToken()
        return consumed
    }

    private fun expectToken(descriptor: TokenDescriptor): Token {
        if (!checkToken(descriptor)) {
            errorExpected(descriptor)
        }
        return consumeToken()
    }

    private fun expectToken(vararg descriptors: TokenDescriptor): Token {
        if (!checkToken(*descriptors)) {
            errorExpected(*descriptors)
        }
        return consumeToken()
    }

    private fun checkToken(descriptor: TokenDescriptor) =
        curToken.descriptor == descriptor

    private fun checkToken(vararg descriptors: TokenDescriptor) =
        curToken.descriptor in descriptors

    private fun errorExpected(vararg descriptors: TokenDescriptor): Nothing {
        error(
            "given ${curToken.errorMessageName()}, but expected one of the following: " +
                    descriptors.joinToString(", ") { it.description }
        )
    }

    private fun errorExpected(message: String): Nothing {
        error("given ${curToken.errorMessageName()}, but expected $message")
    }

    private fun error(message: String): Nothing {
        throw RadkellParserException(lexer.getLastTokenLineNumber(), message)
    }
}
package pl.semidude.radkell.parser.nodes

import pl.semidude.radkell.parser.ConstantDef
import pl.semidude.radkell.parser.RuntimeParamDef
import pl.semidude.radkell.parser.evaluation.ContextParamReader
import pl.semidude.radkell.parser.evaluation.Evaluable
import pl.semidude.radkell.parser.evaluation.SimpleEvaluator
import pl.semidude.radkell.parser.visitors.ParseTreeVisitor

/**
 * Node for reading constant value. The value is statically evaluated by simply evaluating the underlying initializer
 * expression.
 */
class ConstantReadNode(
    private val constantDef: ConstantDef,
    override val lineNumber: Int = 0
) : ExprNode(constantDef.type),
    Evaluable by SimpleEvaluator(constantDef.initializerExpr) {

    override fun accept(visitor: ParseTreeVisitor) = visitor.visit(this)

    override fun contentsToString(): String = "$constantDef"
}

/**
 * Node for reading parameter value (both function parameter and input slot). The evaluation is done by reading
 * from CallContext instance
 */
class RuntimeParamReadNode(
    private val paramDef: RuntimeParamDef,
    override val lineNumber: Int = 0
) : ExprNode(paramDef.type),
    Evaluable by ContextParamReader(paramDef) {

    override fun accept(visitor: ParseTreeVisitor) = visitor.visit(this)

    override fun contentsToString(): String = "$paramDef"
}
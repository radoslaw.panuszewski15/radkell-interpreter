package pl.semidude.radkell.parser.evaluation

import pl.semidude.radkell.lexer.TypeName
import pl.semidude.radkell.lexer.TypeName.*
import pl.semidude.radkell.parser.RuntimeParamDef
import pl.semidude.radkell.parser.nodes.ExprNode

/**
 * Immutable call context abstraction. Contains a map of runtime parameter names to their corresponding values.
 * The only way to change the underlying map of parameters is to create new instance of that class by calling
 * appropriate methods.
 */
@Suppress("UNCHECKED_CAST")
class CallContext(
    private val params: Map<RuntimeParamDef, Any>
) {
    /**
     * Return new [CallContext] instance which includes given parameters. The order of both lists should be the same.
     * @param paramDefs list of parameter definitions
     * @param paramValues list of parameter value expressions to be evaluated
     * @return new instance of [CallContext] which includes given parameters
     */
    fun includeParams(paramDefs: List<RuntimeParamDef>, paramValues: List<ExprNode>): CallContext {
        val callParams: MutableMap<RuntimeParamDef, Any> = HashMap(params)

        for ((i, param) in paramDefs.withIndex()) {
            callParams[param] = paramValues[i].evaluate(this)
        }
        return CallContext(callParams)
    }

    /**
     * Return new [CallContext] instance which includes given parameters
     * @param paramDef parameter definition
     * @param paramValue parameter value
     * @return new instance of [CallContext] which includes given parameter
     */
    fun includeParam(paramDef: RuntimeParamDef, paramValue: Any): CallContext {
        return CallContext(params + Pair(paramDef, paramValue))
    }

    /**
     * Get [Int] parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getIntParam(paramDef: RuntimeParamDef): Int {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, INT)
        return params.getValue(paramDef) as Int
    }

    /**
     * Get [Double] parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getDoubleParam(paramDef: RuntimeParamDef): Double {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, DOUBLE)
        return params.getValue(paramDef) as Double
    }

    /**
     * Get [Boolean] parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getBoolParam(paramDef: RuntimeParamDef): Boolean {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, BOOL)
        return params.getValue(paramDef) as Boolean
    }

    /**
     * Get [List]<[Int]> parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getIntListParam(paramDef: RuntimeParamDef): List<Int> {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, INT_LIST)
        return params.getValue(paramDef) as List<Int>
    }

    /**
     * Get [List]<[Double]> parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getDoubleListParam(paramDef: RuntimeParamDef): List<Double> {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, DOUBLE_LIST)
        return params.getValue(paramDef) as List<Double>
    }

    /**
     * Get [List]<[Boolean]> parameter value
     * @param paramDef parameter to get value of
     * @return value of given parameter
     * @throws IllegalStateException if given parameter is not defined in this context
     * @throws IllegalStateException if given parameter is defined but of wrong type
     */
    fun getBoolListParam(paramDef: RuntimeParamDef): List<Boolean> {
        ensureIsDefined(paramDef)
        ensureIsOfType(paramDef, BOOL_LIST)
        return params.getValue(paramDef) as List<Boolean>
    }

    private fun ensureIsDefined(paramDef: RuntimeParamDef) {
        if (!params.containsKey(paramDef))
            throw IllegalStateException("$paramDef: no such parameter in current call context")
    }

    private fun ensureIsOfType(paramDef: RuntimeParamDef, type: TypeName) {
        if (paramDef.type != type)
            throw IllegalStateException("parameter ${paramDef.name} is not $type")
    }
}
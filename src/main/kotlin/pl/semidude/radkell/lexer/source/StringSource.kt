package pl.semidude.radkell.lexer.source

import pl.semidude.radkell.lexer.source.Source.Companion.EOT

/**
 * [Source] implementation, which gets the source code as [String]
 */
open class StringSource(input: String) : Source {
    private val text = input + '\n' + EOT

    private var readPos = 0
    private var lastCharLine = 1
    override fun hasNext(): Boolean = readPos < text.length

    override fun getNextChar(): Char =
        if (readPos < text.length) {
            val char = text[readPos++]
            if (char == '\n')
                lastCharLine++
            char
        }
        else throw EndOfTextException()

    override fun peekNextChar(): Char =
        if (readPos < text.length) text[readPos]
        else EOT

    override fun ungetChar() {
        if (text[--readPos] == '\n') lastCharLine--
    }

    override fun getLastCharLineNumber(): Int = lastCharLine
}